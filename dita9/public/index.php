<?php
session_start();

error_reporting(E_ALL);

if (!isset($_SESSION['shporta']))
	{
	$_SESSION['shporta'] = array();	
	}

define("THIRRJA", 1);
require_once("../app/config/db.php");
$data = array();
$url = explode("/", $_SERVER['PATH_INFO']);
if (!empty($url[1]))
	{
	$cmd = filter_var($url[1], FILTER_SANITIZE_STRING);
	
	
	switch ($cmd)
		{
			case 'login':
				require '../app/controllers/login.php';
				break;
			case 'check':
				require '../app/controllers/check.php';
				break;
			case 'produktet':
				require '../app/controllers/produktet.php';
				break;						
			case 'produkti':
				require '../app/controllers/produkti.php';
				break;
			case 'shporta':
				require '../app/controllers/shporta.php';
				break;	
			case 'shto':
				require '../app/controllers/shto-ne-shporte.php';
				break;															
			case 'kontakti':
				require '../app/controllers/kontakti.php';
				break;
			case 'signup':
				require '../app/controllers/regjistrimi.php';
				break;							
			default:
				// per auth = true
				if (isset($_SESSION['auth']) && $_SESSION['auth'])
					{
					switch ($cmd)
						{
						case 'profili':
							require '../app/controllers/profili.php';
							break;
						case 'logout':
							require '../app/controllers/logout.php';
							break;
						default:
							//
							if ($_SESSION['level'] == 2)
								{
									switch ($cmd)
										{
										case "anetaret":
										include "../app/controllers/admin/anetaret.php";	
										}
								}
							else
								{
								$data['raporti'] = "Nuk ekziston kjo faqe!";	
								$data['view'] = "raporti";	
								}
						}
					}
				else
					{
					$data['raporti'] = "Nuk ekziston kjo faqe!";	
					$data['view'] = "raporti";
					}
		}
	}
else
	{
	include "../app/controllers/home.php";	
	}

// Mbyllja e koneksionit pas perfundimit te punes se kontrollereve
mysqli_close($dbcon);
	
// konvertimi i data array ne variabla te rendomta
/*foreach($data as $key=>$value)
	{
		$$key = $value;
	}
	*/
	extract($data);

// thirrja e template kryesor
include('../app/views/layout.php');