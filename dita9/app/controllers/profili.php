<?php if ( ! defined('THIRRJA')) exit('Nuk keni qasje direkte'); ?>

<?php
if (isset($_SESSION['auth']) && $_SESSION['auth'])
	{
	$data['title'] = "Profili";	
	$data['emri'] = $_SESSION['emri'];
	$data['view'] = "profili";	
	}
else
	{
	$data['title'] = "Gabim";
	$data['raporti'] = "Nuk jeni te regjistruar, identifokuni <a href='login.php'>ketu</a>";
	$data['view'] = "raporti";	
	}
?>