-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2015 at 04:19 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ickshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cat_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Cat_ID`),
  KEY `Cat_Name` (`Cat_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Cat_ID`, `Cat_Name`) VALUES
(2, 'Kompjuter'),
(1, 'Laptop'),
(4, 'Printer'),
(3, 'Skener');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `Man_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Man_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Man_ID`),
  KEY `Man_Name` (`Man_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`Man_ID`, `Man_Name`) VALUES
(2, 'Canon'),
(5, 'Dell'),
(1, 'HP'),
(4, 'SONY'),
(3, 'Toshiba');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `Pro_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Pro_Cat_ID` int(11) NOT NULL,
  `Pro_Man_ID` int(11) NOT NULL,
  `Pro_Code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Price` decimal(6,2) NOT NULL,
  `Pro_Available` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Pro_ID`),
  KEY `Pro_Cat_ID` (`Pro_Cat_ID`,`Pro_Man_ID`,`Pro_Code`,`Pro_Name`,`Pro_Price`,`Pro_Available`),
  FULLTEXT KEY `Pro_Description` (`Pro_Description`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Pro_ID`, `Pro_Cat_ID`, `Pro_Man_ID`, `Pro_Code`, `Pro_Name`, `Pro_Description`, `Pro_Price`, `Pro_Available`) VALUES
(1, 1, 5, '00021542', 'Inspiron 17R', 'af asdf asf asd fas dfas dfas\r\ndfas\r\ndf asd a sdf asdf asdfas df\r\nasd fasd\r\nfasd\r\nf\r\nasd\r\nfasdf ', '650.15', 1),
(2, 1, 4, '54565858', 'VAIO', 'asdfasdfasd as fd asdf as dfas fas f asdfas\r\ndfas\r\ndfa\r\nsdf\r\nasdf asdf asdf asdf\r\n', '999.95', 1),
(3, 1, 3, '54545', 'Satellite XP987', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '1100.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `Usr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usr_Email` varchar(50) NOT NULL,
  `Usr_Password` varchar(40) NOT NULL,
  `Usr_Name` varchar(20) NOT NULL,
  `Usr_Level` int(11) NOT NULL DEFAULT '0',
  `Usr_Salt` int(11) NOT NULL,
  PRIMARY KEY (`Usr_ID`),
  UNIQUE KEY `Usr_Email` (`Usr_Email`),
  KEY `Usr_Password` (`Usr_Password`),
  KEY `Usr_Level` (`Usr_Level`),
  KEY `Usr_Salt` (`Usr_Salt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Usr_ID`, `Usr_Email`, `Usr_Password`, `Usr_Name`, `Usr_Level`, `Usr_Salt`) VALUES
(1, 'tahir.hoxha@gmail.com', 'cf9be0086bf4502449a6a933fa5a784593cbf5e7', 'Tahir Hoxha', 2, 8601989),
(2, 'korab@korab.com', '98c5a3012cfb0a19ec1da2ba462cbecc6bceb7fd', 'Korab', 1, 7538787),
(3, 'zanfina@hotmail.com', '393c1bab533f7df51344d8600ce81ef3e7716248', 'Zanfina', 0, 7368225);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
