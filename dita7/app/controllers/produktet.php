<?php

$sql = "SELECT * FROM `products` 
INNER JOIN manufacturers ON Man_ID = Pro_Man_ID
INNER JOIN categories ON Cat_ID = Pro_Cat_ID";

$results = mysqli_query($dbcon, $sql);

if ($results)
	{
	$data['tabela'] =  "<table class='table'>";
	while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
		{
			$Pro_ID =  $row['Pro_ID'];
			$Pro_Code =  $row['Pro_Code'];
			$Pro_Name = $row['Pro_Name'];
			$Pro_Description = $row['Pro_Description'];
			$Pro_Price = $row['Pro_Price'];
			$Cat_Name = $row['Cat_Name'];
			$Pro_Cat_ID = $row['Pro_Cat_ID'];
			$Man_Name = $row['Man_Name'];
			$Pro_Man_ID = $row['Pro_Man_ID'];
			
			$data['tabela'] .= "<tr>
			<td>".$Pro_ID."</td>
			<td>".$Pro_Name."</td>
			<td>".$Pro_Price."</td>
			<td>".$Cat_Name."</td>
			<td>".$Man_Name."</td>
			</tr>";
		}
		
		$data['tabela'] .= "</table>";
		$data['title'] = "Lista e produkteve";
		$data['view'] = "produktet";
	}
else
	{
	// NUK KA SHENIME	
	}
