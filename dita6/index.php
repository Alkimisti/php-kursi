<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="assets/css/stili.css">
</head>

<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>Online Shop</h1>
<nav>
<ul>
<li><a href='/'>Home</a></li>

<li><a href='/login'>Login</a></li>
<li><a href='/profili'>Profili</a></li>
<li><a href='/kontakti'>Kontakti</a></li>
<li><a href='/logout'>Logout</a></li>
<li><a href='/signup'>Regjistrohu</a></li>
</ul>
</nav>
<?php

$url = explode("/", $_SERVER['PATH_INFO']);

define("THIRRJA", 1);
session_start();
require_once("db.php");

if (!empty($url[1]))
	{
	// $cmd = filter_var($_GET['cmd'], FILTER_SANITIZE_STRING);
	$cmd = filter_var($url[1], FILTER_SANITIZE_STRING);
	
	
	switch ($cmd)
		{
			case 'login':
				require 'login.php';
				break;
			case 'check':
				require 'check.php';
				break;	
			case 'produkti':
				require 'produkti.php';
				break;							
			case 'kontakti':
				require 'kontakti.php';
				break;
			case 'signup':
				require 'regjistrimi.php';
				break;							
			default:
				// per auth = true
				if (isset($_SESSION['auth']) && $_SESSION['auth'])
					{
					switch ($cmd)
						{
						case 'profili':
							require 'profili.php';
							break;
						case 'logout':
							require 'logout.php';
							break;
						default:
							//
							if ($_SESSION['level'] == 2)
								{
									switch ($cmd)
										{
										case "anetaret":
										include "admin/anetaret.php";	
										}
								}
							else
								{
								echo "Kjo faqe nuk ekziston!";	
								}
						}
					}
				else
					{
					echo "Nuk ekziston kjo faqe!";	
					}
				
				
		}
	}
else
	{
	require 'home.php';	
	}



?>
</div>
</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js">
</body>
</html>