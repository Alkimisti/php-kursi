<?php
$sql = "SELECT * FROM products
INNER JOIN categories ON Pro_Cat_ID = Cat_ID
INNER JOIN manufacturers ON Pro_Man_Id = Man_ID
WHERE 1=1";
	
if ($_SERVER['REQUEST_METHOD'] == "POST")
	{
	$emri = filter_var($_POST['emri'], FILTER_SANITIZE_STRING);
	$cmimi1 = abs(floatval($_POST['cmimi1']));
	$cmimi2 = abs(floatval($_POST['cmimi2']));
	$kategoria = intval($_POST['kategoria']);
	
	if (isset($_POST['prodhuesi']))
		{
		$prodhuesi = array();
		foreach ($_POST['prodhuesi'] as $key=>$value)
			{
			$prodhuesi[] = intval($value);	
			}
		}
	
	$renditja = intval($_POST['renditja']);
	
	$query1 = "";
	if (strlen($emri) > 0)
		{
		$query1 = " AND Pro_Name LIKE '%".$emri."%' ";	
		}
		
	if ($cmimi1 > 0 && $cmimi2 > 0 && $cmimi1 > $cmimi2)
		{
		$x = $cmimi1;
		$cmimi1 = $cmimi2;
		$cmimi2 = $x;	
		}
	
	$query2 = "";	
	if ($cmimi1 > 0)
		{
		$query2 = " AND Pro_Price >= '".$cmimi1."' ";	
		}
	
	$query3 = "";	
	if ($cmimi2 > 0)
		{
		$query3 = " AND Pro_Price <= '".$cmimi2."' ";	
		}	
		
	$query4 = "";	
	if ($kategoria > 0)
		{
		$query4	= " AND Pro_Cat_ID = '".$kategoria."' ";
		}
		
	$query5 = "";	
	/*foreach($prodhuesi as $pr_id)
		{
		$query5 .= " OR Pro_Man_ID = '".$pr_id."' ";	
		}
	
	if ($query5 != "")
		{
		$query5 = " AND (".substr($query5, 3, strlen($query5)-4).") ";
		}*/
		
	if ($prodhuesi[0] > 0)
		{
		$query5 = " AND (Pro_Man_ID=".implode(" OR Pro_Man_ID=", $prodhuesi).") ";	
		}
		
	$opsionet = array(1=> " ORDER BY Pro_Price ASC ", 2=> " ORDER BY Pro_Price DESC ",  3=> " ORDER BY Pro_Date DESC ", 4=> " ORDER BY Pro_Date ASC ");
	
	$query6 = $opsionet[$renditja];
	
	$_SESSION['kerkesa'] = $shtesa = $query1.$query2.$query3.$query4.$query5.$query6;
	$sql = $sql . $shtesa;
	}
else
	{
	// futja me get	
	$sql = $sql . $_SESSION['kerkesa'];
	}


$sql_total = "SELECT COUNT(Pro_ID) AS Totali FROM products
INNER JOIN categories ON Pro_Cat_ID = Cat_ID
INNER JOIN manufacturers ON Pro_Man_Id = Man_ID
WHERE 1=1".$_SESSION['kerkesa'];
$results  = mysqli_query($dbcon, $sql_total);
$rezulati = array();
while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
	{
		$Totali = $row['Totali'];
	}

// Interpretimi i URL per pagination
$per_page = 5;
$page = ($cmd2==0) ?  1: intval($cmd2); 
$from = ($page-1)*$per_page;




$results  = mysqli_query($dbcon, $sql." LIMIT $from, $per_page");	












if (mysqli_num_rows($results) > 0)
	{
	$rezulati = array();
	while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
		{
			$rezulati[] = array(
			'Pro_ID' =>  $row['Pro_ID'],
			'Pro_Name' => $row['Pro_Name'],
			'Pro_Price' => $row['Pro_Price'],
			'Cat_Name' => $row['Cat_Name'],
			'Pro_Cat_ID' => $row['Pro_Cat_ID'],
			'Man_Name' => $row['Man_Name'],
			'Pro_Man_ID' => $row['Pro_Man_ID']
			);
			}
		
		$data['produktet'] = $rezulati;
		$data['title'] = "Lista e produkteve";
		echo $twig->render('rezultatet.html', $data);
		}
	else
		{
		$data['raporti'] = "Nuk ka shenime";
		echo $twig->render('raporti.html', $data);	
		}
		