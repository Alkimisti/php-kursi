<?php
if ($_SERVER['REQUEST_METHOD'] == "POST")
	{
	$_SESSION['sql'] = "";
	$query1=""; 
	$query2=""; 
	$query3=""; 
	$query4=""; 
	$order="";
	
	$emri = filter_var($_POST['emri'], FILTER_SANITIZE_STRING);
	$cmimi1 = intval($_POST['cmimi1']);
	$cmimi2 = intval($_POST['cmimi2']);
	$kategoria = $_POST['kategoria'];
	$prodhuesi = $_POST['prodhuesi'];
	$renditja = intval($_POST['renditja']);
		
		
	if (strlen($emri) > 0) {
		$query1 = " AND Pro_Name LIKE '%$emri%' ";
		}
	
	
	if ($cmimi1!=0 && $cmimi2!=0 && $cmimi1>$cmimi2)
		{
		$x = $cmimi1; 
		$cmimi1 = $cmimi2;
		$cmimi2 = $x;	
		}
					
	if ($cmimi2 > 0)
		{		
		$query2 = " AND (Pro_Price >= $cmimi1 AND Pro_Price <= $cmimi2) ";
		}
	else
		{
		$query2 = " AND Pro_Price >= $cmimi1 ";	
		}
			
	if ($kategoria > 0)
		{
		$query3 = " AND Pro_Cat_ID = '$kategoria' ";
		}
	
			
	if ($prodhuesi[0] > 0)
		{
		$query4 = implode(" OR Pro_Man_ID = ", $prodhuesi);	
		$query4 = "AND (Pro_Man_ID = ".$query4.") ";
		}
		
		switch($renditja)
			{
			case 1:
				$order = " ORDER BY Pro_Price ASC ";
				break;
			case 2:
				$order = " ORDER BY Pro_Price DESC ";
				break;
			case 3:
				$order = " ORDER BY Pro_Date ASC ";
				break;
			case 4:
				$order = " ORDER BY Pro_Date DESC ";
				break;												
					
			}
			
				
		$_SESSION['sql'] = $query1.$query2.$query3.$query4.$order;	
					
	}

include('kerkimi_model.php');
	
// Interpretimi i URL per pagination
$per_page = 5;
$page = ($cmd2==0) ?  1: intval($cmd2); 
$from = ($page-1)*$per_page;


// Gjenerimi i linkave per pagination
$totali = total_produkte_kerkuara_av($dbcon, $_SESSION['sql']);	

$pages = ceil($totali/$per_page);
$data['links'] = "<nav><ul class='pagination'>";
for ($i = 1; $i <= $pages; $i++)
	{
	if ($i == $page)
		{
		$data['links'] .= "<li class='active'><a href='/rezultatet/".$i."'>".$i."</a></li>";
		}
	else
		{
		$data['links'] .= "<li><a href='/rezultatet/".$i."'>".$i."</a></li>";
		}
	}
$data['links'] .= "</ul></nav>";		
	
$data['produktet'] = kerkoav_produktet($dbcon, $_SESSION['sql'], $from, $per_page);

if (!empty($data['produktet'])) 
	{
	$data['title'] = "Lista e produkteve";
	echo $twig->render('rezultatet.html', $data);
	}
else
	{
	$data['raporti'] = "Nuk ka shenime";
	echo $twig->render('raporti.html', $data);
	}
			