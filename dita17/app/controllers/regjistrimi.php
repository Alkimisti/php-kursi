<?php if ( ! defined('THIRRJA')) exit('Nuk keni qasje direkte');

$data['raporti'] = "";

if ($_SERVER['REQUEST_METHOD'] == "POST")
	{
	$emri = trim($_POST['emri']);
  	$emaili = trim($_POST['emaili']);
  	$pass1 = trim($_POST['password1']);
  	$pass2 = trim($_POST['password2']);
	
	if ($pass1 != $pass2)
		{
		$data['raporti'] = "Passwordat nuk jane te njejte";
		}
		
	if (strlen($data['raporti']) > 0)
		{
		echo $twig->render('raporti.html', $data);	
		}
	else 
		{
		$random = rand(1000000, 9999999);
		$passhash = sha1($random.$pass1);
		
		$sql = "INSERT INTO users (Usr_Email, Usr_Password, Usr_Name, Usr_Salt) VALUES ('$emaili', '$passhash', '$emri', '$random')";
		
	
		
		$results = mysqli_query($dbcon, $sql);

		 
		if ($results)
			{
			 
			$last_id = mysqli_insert_id($dbcon);
			$hash = sha1($last_id.$random);
			
			$message = "<h1>Aktivizimi i llogarisë</h1><p>Na ka arritur nje kerkese per regjistrim ne faqen tone www.hide.al. Per te qene ne gjendje ta shfrytezoni sajtin tone, ju duhet ta aktivizoni llogarine tuaj. Aktivizimi kryhet duke klikuar ne linkun vijues:</p>";
			$message .= "<p><a href='".$_SERVER['HTTP_HOST']."/aktivizo/".$hash."'>Kliko ketu</a></p>";
			$message .= "<p>http://".$_SERVER['HTTP_HOST']."/aktivizo/".$hash."</p>";
			$message .= "<p>Nese linku nuk eshte i klikueshem, kopjojeni rreshtin dhe vendosen ne address bar.</p>";
			$message .= "<p>Me respekt,</p><p>Stafi i hide.al</p>";
			
			//echo $message;
			
			dergo($emaili, "Aktivizimi i llogarise suaj ne hide.al", $message);	
			$data['raporti'] = "Ju u regjistruat. Ne email adresen tuaj eshte derguar kodi i aktivizimit. Pas klikimit te linkut te ofruar ne email, ju do te mund te beni login ne sajtin tone.";
			}
		else
			{
			$data['raporti'] = "Gabim gjate ekzekutimit te kerkeses!";	
			
			}
		}
	echo $twig->render('raporti.html', $data);
	}
else
	{
	$data['title'] = "Regjistrimi i anetareve";
	echo $twig->render('regjistrimi.html', $data);
	}