# COOKIE #

Në këtë skriptë, vlerat e 3 variablave janë vendosur në një cookie, në formë të një vargu (array) me 3 elemente.

Meqenëse cookie i ruan të dhënat në formë tekstuale, nuk është e mundur të ruhen vargjet (arrays), si struktura më komplekse të të dhënave që janë. Prandaj, paraprakisht nevojitet të bëhet **serializimi** i vargut me *serialize()*, që në fakt është transformimi i vargut në një **string** të formatit special. Gjatë procesit të leximit, bëhet e kundërta: nga **string** do ta kthejmë në **array** me *unserialize()*.

Variablat e përdorur janë: $emri, $emaili, $komenti.

Në këtë variant, atyre nuk iu qasemi me:

    $_COOKIE['emri']
    $_COOKIE['emaili']
    $_COOKIE['komenti'] 

Por, pas:

    $vlerat = unserialize($_COOKIE['vlerat'])


i lexojme me: 

    $vlerat['emri']
    $vlerat['emaili']
    $vlerat['komenti']




Pra, **serialize()** e kthen vargun në tekst, ndërsa **unserialize()**  - tekstin në varg.