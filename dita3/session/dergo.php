<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<?php
session_start();
$emri = trim(filter_var($_POST['emri'], FILTER_SANITIZE_STRING));
if (strlen($emri) == 0)
	{
	echo "<p>Nuk e keni shenuar emrin!</p>";	
	}
else
	{
	echo "<p>Emri: ".$emri."</p>";	
	}


$emaili = $_POST['emaili'];
if (!filter_var($emaili, FILTER_VALIDATE_EMAIL))
	{
	echo "<p>Emaili nuk eshte i shkruar mire!</p>";	
	}
else
	{
	echo "<p>Emaili: ".$emaili."</p>";	
	}

$komenti = $_POST['komenti'];
if (strlen($komenti) == 0)
	{
	echo "<p>Nuk e keni shenuar komentin!</p>";		
	}
else
	{
	echo "<div>".nl2br($komenti)."</div>";
	}
$_SESSION['emri']  = $emri;
$_SESSION['emaili']  = $emaili;
$_SESSION['komenti']  = $komenti;
echo "<p><a href='formulari.php'>Kthehu te formulari</a></p>";
?>
</body>
</html>
