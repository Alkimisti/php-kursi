<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<?php

$emri = trim(filter_var($_POST['emri'], FILTER_SANITIZE_STRING));
if (strlen($emri) == 0)
	{
	echo "<p>Nuk e keni shenuar emrin!</p>";	
	}
else
	{
	echo "<p>Emri: ".$emri."</p>";	
	}


$emaili = $_POST['emaili'];
if (!filter_var($emaili, FILTER_VALIDATE_EMAIL))
	{
	echo "<p>Emaili nuk eshte i shkruar mire!</p>";	
	}
else
	{
	echo "<p>Emaili: ".$emaili."</p>";	
	}

$komenti = $_POST['komenti'];
if (strlen($komenti) == 0)
	{
	echo "<p>Nuk e keni shenuar komentin!</p>";		
	}
else
	{
	echo "<div>".nl2br($komenti)."</div>";
	}
?>
<form action="formulari.php" method="post">
<input type="hidden" name="emri" value="<?=$emri;?>">
<input type="hidden" name="emaili" value="<?=$emaili;?>">
<input type="hidden" name="komenti" value="<?=$komenti;?>">
<input type="submit" value="Kthehu te formulari" >
</form>
</body>
</html>
