# CMS #

Shembull për të ilustruar leximin e ID-së së lajmit nga URL-ja.

Fillimisht gjenerohet lista e titujve në mënyrë dinamike. në rastin konkret duke iteruar me **for** nëpër vargun $titujt. Në një Web aplikacion real, leximi bëhet nga një SQL tabelë që i përmban titujt.

**for** duhet të zëvendësohet me **foreach** për të mos pasur nevojë të ceket numri i elementeve të vargut, por këtu shërben vetëm si ilustrim i strukturës **for**.

    echo "<ul>";
    for ($x=1; $x<=3; $x++)
    	{
    	echo "<li><a href='cms.php?id=".$x."'>".$titujt[$x]."</a></li>";	
    	}
    echo "</ul>";
Pjesa më esenciale në këtë **snippet** është cikli që e formon listën e titujve, duke gjeneruar linqe në mënyrë dinamike. Variabli **$x** që shërben si ID i lajmit i bashkangjitet si **query string** atributit **href** të elementit **a**, në secilin iteracion me vlerë të re: 1, 2, 3.

Në këtë mënyrë fitojmë segmentin e parë të linkut:


- Në iteracionin e parë: **`<li><a href='cms.php?id=1'>`**
- Në iteracionin e dytë: **`<li><a href='cms.php?id=2'>`**
- Në iteracionin e tretë: **`<li><a href='cms.php?id=3'>`**

Segmenti i dytë fitohet duke bashkangjitur titullin përkatës nga vargu **titujt**.

- Në iteracionin e parë: **`Krizë e re e pengjeve në Francë</a></li>`**
- Në iteracionin e dytë: **`Bankers do të ndërpresë prodhimin e naftës në disa puse</a></li>`**
- Në iteracionin e tretë: **`Shpërthen bomba e Luftës së Dytë Botërore</a></li>`**


Me bashkimin e këtyre segmenteve, fitojmë këto URL:

- **`<li><a href='cms.php?id=1'>Krizë e re e pengjeve në Francë</a></li>`**
- **`<li><a href='cms.php?id=2'>Bankers do të ndërpresë prodhimin e naftës në disa puse</a></li>`**
- **`<li><a href='cms.php?id=3'>Shpërthen bomba e Luftës së Dytë Botërore</a></li>`**

Këto 3 rreshta, në browser shfaqen si 3 linqe:

- [Krizë e re e pengjeve në Francë](http://localhost/cms.php?id=1)
- [Bankers do të ndërpresë prodhimin e naftës në disa puse](http://localhost/cms.php?id=2)
- [Shpërthen bomba e Luftës së Dytë Botërore](http://localhost/cms.php?id=3)

   

Kur vizitori e klikon një nga këto linqe, faqja **cms.php** pranon një GET variabël nëpërmes **query string**. 

Marrim shembull linkun e parë:
**http://localhost/cms.php?id=1**

Brenda skriptës, vlera e shënuar pas **id** lexohet me:

    $_GET['id']

Duke dëshiruar të parandalojmë shfaqjen e raportit të gabimit në situatat kur **id** mungon, ne e bëjmë leximin e kësaj vlere në këtë mënyrë:

    $id = (isset($_GET['id']))? $_GET['id'] : 1;


Ky quhet **ternary operator** dhe është një mënyrë koncize për shkrimin e një kushti. Kjo me **if** do të shkruhej:

    if (isset($_GET['id'])) {
    $id = $_GET['id'];
    }
    else {
    $id = 1;
    }

Kushti është: a ekziston GET variabli id?

Nëse ekziston, variablit **$id** i përcillet vlera e **$_GET['id']**. Në të kundërtën i përcillet vlera **1**. Kjo do të thotë se nëse në URL mungon ID-ja, do të përdoret vlera **1**, dmth do të zgjedhet lajmi **1** për t'u shfaqur më pas.

Shfaqja e lajmit bëhet me këto 2 rreshta:

    echo "<h1>".$titujt[$id]."</h1>";
    echo "<div>".$lajmet[$id]."</div>";

Rreshti i parë e "printon" titullin, të vendosur brenda tag-ut **h1**, ndërsa rreshti i dytë e tregon tekstin e lajmit të vendosur brenda tag-ut **div**.


Nëse psh. URL-ja ka qenë:

**[http://localhost/cms.php?id=3](http://localhost/cms.php?id=3)**

atëherë do të shfaqet lajmi i tretë nga vargu.

#Krizë e re e pengjeve në Francë

Thuhet se lindi një krizë e re e pengjeve në një argjendari në Montpellier në jug të Francës. Nuk dihet nëse ka ndërlidhje me sulmet terroriste. Sipas disa raporteve, kriza u krijua pasi plaçkitja nuk eci si duhet. Policia është në vendngjarje.
