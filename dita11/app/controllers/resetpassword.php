<?php if ( ! defined('THIRRJA')) exit('Nuk keni qasje direkte'); ?>

<?php
if ($_SERVER['REQUEST_METHOD'] == "GET")
	{
	$data['title'] = "Resetimi i passwordit";		
	$data['view'] = "resetpassword";
	}
else
	{
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		{
		$email = trim(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
		$sql = "SELECT * FROM users 
		WHERE Usr_Email = '$email'
		LIMIT 1";
		$results = mysqli_query($dbcon, $sql);
		if (mysqli_num_rows($results) > 0)
			{
			while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
				{
				$Usr_ID =  $row['Usr_ID'];
				$Usr_Name =  $row['Usr_Name'];
				$Usr_Email =  $row['Usr_Email'];
				}
			
			$random = rand(1111111,9999999);
			
			$sql = "UPDATE users SET Usr_Random = '$random' WHERE Usr_ID = '$Usr_ID' LIMIT 1";
			$results = mysqli_query($dbcon, $sql);
			$unik = "http://dita11.dev/reset/".sha1($Usr_ID.$random);	
			$to = $Usr_Email;
			$subject = "Kerkese per ndryshimin e passwordit";
			$message = "Kemi marre kerkese per ndryshimin e passwordit tuaj. Nese ju e keni bere kerkesen, klikoni ne linkun me poshte:";
			$message .=  "<p><a href='$unik'>Kliko ketu</a></p>";
			$message .=  "<p>$unik</p>";
			
			
			$message .= "<p>Nese ju nuk e keni bere kerkesen, injorojeni ose fshijeni kete mesazh.</p>";
			
			if (dergo($to, $subject, $message))
				{
			$data['raporti'] = "Emaili me linkun per resetimin e passwordit u dergua! Kontrollojeni emailin tuaj.";			
				}
			else
				{
				$data['raporti'] = "Gabim gjate dergimit te emailit";	
				}
			
			}
		else
			{
			$data['raporti'] = "Gabim!";	
			}
		}
	else
		{
		$data['raporti'] = "Nuk keni shenuar email adrese valide!";	
		}
		
	$data['title'] = "Resetimi i passwordit";		
	$data['view'] = "raporti";	
	}