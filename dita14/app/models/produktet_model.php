<?php

function total_produkte($dbcon) {
$sql = "SELECT COUNT(Pro_ID) AS Totali FROM products";
$results = mysqli_query($dbcon, $sql);
while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
		{
			$totali = $row['Totali'];
		}
	return $totali;
	
}

function trego_produktet($dbcon, $from=1, $rows=5) {
	
$sql = "SELECT * FROM `products` 
INNER JOIN manufacturers ON Man_ID = Pro_Man_ID
INNER JOIN categories ON Cat_ID = Pro_Cat_ID
ORDER BY Pro_ID DESC
LIMIT $from, $rows";

$results = mysqli_query($dbcon, $sql);

if (mysqli_num_rows($results) > 0)
	{
	$rezulati = array();
	while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
		{
			$rezulati[] = array(
			'Pro_ID' =>  $row['Pro_ID'],
			'Pro_Name' => $row['Pro_Name'],
			'Pro_Price' => $row['Pro_Price'],
			'Cat_Name' => $row['Cat_Name'],
			'Pro_Cat_ID' => $row['Pro_Cat_ID'],
			'Man_Name' => $row['Man_Name'],
			'Pro_Man_ID' => $row['Pro_Man_ID']
			);
			}
		return  $rezulati;
		}
	else
		{
		return NULL;	
		}
}