<?php

/* regjistrimi.html */
class __TwigTemplate_e63e1d04c0817dff44883af36a3de0cb7f0276116f5dd637038f4ef1c5ec381a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action=\"/signup\" method=\"post\">
<table class=\"table\">
  <tr>
    <td>Emri dhe mbiemri</td>
    <td>
      <input class=\"form-control\" type=\"text\" name=\"emri\" id=\"emri\"></td>
  </tr>
  <tr>
    <td>Emaili</td>
    <td>
      <input class=\"form-control\" type=\"email\" name=\"emaili\" id=\"emaili\"></td>
  </tr>
  <tr>
    <td>Passwordi</td>
    <td>
      <input class=\"form-control\" type=\"password\" name=\"password1\" id=\"password1\"></td>
  </tr>
  
 <tr>
    <td>Konfirmo passwordin</td>
    <td>
      <input class=\"form-control\" type=\"password\" name=\"password2\" id=\"password2\"></td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td><input type=\"submit\" name=\"submit\" id=\"submit\" value=\"Submit\"></td>
  </tr>
</table>
<p>&nbsp;</p>
<p><input class=\"btn btn-success\" type=\"submit\" value=\"Regjistrohu\"> 
</form>
";
    }

    public function getTemplateName()
    {
        return "regjistrimi.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
