<?php

/* kontakti.html */
class __TwigTemplate_7b5e3173a2f599ff9b20ca93e10dccbb739e9e77424a194ec2310efbfe046f1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "<h3>Kontakti</h3>
<div class=\"row\">
\t<div class=\"col-md-12\">
    <div class=\"col-md-4\">Adresa</div>
    <div class=\"col-md-8\">rr.Rexhep Mala nr. xxx</div>
    
    <div class=\"col-md-4\">Qyteti</div>
    <div class=\"col-md-8\">Prishtine</div>
    
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "kontakti.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 7,  45 => 6,  40 => 3,  37 => 2,  11 => 1,);
    }
}
