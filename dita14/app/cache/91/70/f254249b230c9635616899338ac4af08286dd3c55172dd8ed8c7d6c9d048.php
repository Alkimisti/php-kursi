<?php

/* porosit.html */
class __TwigTemplate_9170f254249b230c9635616899338ac4af08286dd3c55172dd8ed8c7d6c9d048 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Ju faleminderit!</h1>
<h2>Porosia juaj</h2>

<table class='table'>
<tr><th>ID</th>
<th>Emertimi</th>
<th>Cmimi</th>
<th>Sasia</th>
<th>Vlera</th>
</tr>

";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabela"]) ? $context["tabela"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["rr"]) {
            // line 16
            echo "<tr>
\t\t<td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "id", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "proname", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "cmimi", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "qty", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "vlera", array()), "html", null, true);
            echo "</td>
\t\t</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rr'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        
<tr><td>&nbsp;</td>
\t\t<td>&nbsp;</td>
\t\t<td>&nbsp;</td>
\t\t<td>Total:</td>
\t\t<td><strong>";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["shuma"]) ? $context["shuma"] : null), "html", null, true);
        echo "</strong></td>
\t\t</tr>
</table>        \t
\t


";
    }

    public function getTemplateName()
    {
        return "porosit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 28,  84 => 23,  75 => 21,  71 => 20,  67 => 19,  63 => 18,  59 => 17,  56 => 16,  52 => 15,  39 => 4,  36 => 3,  11 => 1,);
    }
}
