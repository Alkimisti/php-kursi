<?php

/* kerkimi.html */
class __TwigTemplate_6e76b7780ca992c098a90e5aa11d9585863e423f2ee2fe54da2e8a62c6c96c84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Kërkimi</h1>
     <div>";
        // line 6
        echo (isset($context["links"]) ? $context["links"] : null);
        echo "</div>
    <table class=\"table\">
    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produktet"]) ? $context["produktet"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 9
            echo "    <tr>
    <td>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Price", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Cat_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_Name", array()), "html", null, true);
            echo "</td>
    <td><a class='btn btn-success' href='/produkti/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "'>Hape</a></td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    </table>
   
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "kerkimi.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 18,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  60 => 11,  56 => 10,  53 => 9,  49 => 8,  44 => 6,  39 => 3,  36 => 2,  11 => 1,);
    }
}
