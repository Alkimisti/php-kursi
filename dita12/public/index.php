<?php
session_start();

error_reporting(E_ALL);
$data = array();

if (!isset($_SESSION['shporta']))
	{
	$_SESSION['shporta'] = array();	
	}
	
if (isset($_SESSION['auth']) && $_SESSION['auth'])
	{
	$data['auth'] = TRUE;
	}
else
	{
	$data['auth'] = FALSE;	
	}

// Defino konstanten qe do te perdoret per verifikimin nese kontrolleri eshte thirrur direkt
define("THIRRJA", 1);

// Pergatiti path te foldereve kryesore, ne menyre qe te mund te thirren skriptat e tyre pa path
$path  = PATH_SEPARATOR .'../app/libraries/vendor';
$path .= PATH_SEPARATOR .'../app/config';
$path .= PATH_SEPARATOR .'../app/controllers';
$path .= PATH_SEPARATOR .'../app/includes';

// Lexo vleren aktuale te path dhe shtoja $path qe e definuam me siper
set_include_path(get_include_path() . $path);

// Lexoje fajllin autoload.php. Ky fajll gjendet ne '..\app\libraries\vendor'. Me kete mundesohet leximi automatik i te gjitha pakove qe i kemi marre me Composer, sa here qe paraqitet nevoja per to.
require("autoload.php");

// Defino kredencialet per akses ne MySQL dhe konektohu ne server
require_once("db.php");

// Lexo vlerat e parametrave te konfigurimit
require_once("config.php");

// Lexo biblioteken e funksioneve qe i kemi definuar vete
require_once("functions.php");

// Twig
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('../app/views');
$twig = new Twig_Environment($loader, array(
    'cache' => '../app/cache', 
	'debug' => true
));


// $data variabli do te perdoret per bartjen e vlerave ne template (views). Eshte matrice dhe fillimisht eshte matrice e zbrazet. Askund ne aplikacionin tone nuk duhet ta perdorim variablin $data, pervec kur na duhet ta ruajme nje vlere qe do t'i percillet fajllave te template.


// Zbertheje URL ne segmente, gjithkund ku ka / (slash)
$url = explode("/", $_SERVER['PATH_INFO']);

// Nese ekziston segmenti i pare, pra psh. /kontaktim atehere shko me tutje
if (!empty($url[1]))
	{
	// filtroje tekstin nga segmenti i pare, per te parandaluar futjen e inputit potencialisht te rrezikshem
	$cmd = filter_var($url[1], FILTER_SANITIZE_STRING);
	// Lexo dhe filtro segmentin e dyte nese ekziston
	$cmd2 = (!empty($url[2])) ? filter_var($url[2], FILTER_SANITIZE_STRING): "";

	// Analizo vleren e segmentit te pare, tani te ruajtur ne variablin $cmd
	switch ($cmd)
		{
			// Keto "komanda" vlejne per te gjithe vizitoret e faqes.
			// Nese e ke vleren 'login'...
			case 'login':
				// ...atehere kerkoje fajllin 'login.php'
				require 'login.php';
				break;
			case 'check':
				require 'check.php';
				break;
			case 'produktet':
				require 'produktet.php';
				break;						
			case 'produkti':
				require 'produkti.php';
				break;
			case 'kontakti':
				require 'kontakti.php';
				break;
			case 'signup':
				require 'regjistrimi.php';
				break;	
			case 'aktivizo':
				require 'aktivizo.php';
				break;	
			case 'resetpassword':
				require 'resetpassword.php';
				break;
			case 'reset':
				require 'reset.php';
				break;														
			default:
				// Verifiko a eshte anetar i autentikuar
				if (isset($_SESSION['auth']) && $_SESSION['auth'])
					{
						// Nese PO, atehere, vazhdo me procesimin e "komandave" te tjera, gjegjesisht te vlerave te lexuar nga segmenti i pare i URL
					switch ($cmd)
						{
						// Pra, qasje ne 'shporta', 'shto'... kane vetem anetaret e autentikuar, dmth ata qe kane bere login
						case 'shporta':
							require 'shporta.php';
							break;	
						case 'shto':
							require 'shto-ne-shporte.php';
							break;																						
						case 'porosit':
							require 'porosit.php';	
							break;													
						case 'profili':
							require 'profili.php';
							break;
						case 'logout':
							require 'logout.php';
							break;
						case 'admin':
							// Verifiko nese anetari i autentikuar e ka nivelin 2, dmth a eshte administrator
							
						if ($_SESSION['level'] == 2)
								{
								switch ($cmd2)
										{
										// Ne kete faqe vetem administratori ka qasje
										case "anetaret":
										include  'admin/anetaret.php';	
										break;
										default:
										// raporti per joadministratoret
											$data['raporti'] = "Nuk ekziston kjo faqe";	
								$data['view'] = "raporti";
										
										}
								
								}
							break;
							
							default:
							// Nuk ekziston segmenti i shenuar
							// psh /tralala
								$data['raporti'] = "URL invalide!";	
								$data['view'] = "raporti";	
									
						}
					}
				else
					{
					// anetari nuk eshte autentikuar
					// prandaj nuk ka qasje ne keto faqe
					$data['raporti'] = "Nuk jeni të autorizar!";	
					$data['view'] = "raporti";
					}
		}
	}
else
	{
	// Lexo home page, nese nuk ekziston segmenti 1, pra nese adresa eshte shenuar si http://domaini.com
	include 'home.php';	
	}

// Mbyllja e koneksionit pas perfundimit te punes se kontrollereve
mysqli_close($dbcon);
