<?php if ( ! defined('THIRRJA')) exit('Nuk keni qasje direkte'); ?>

<?php
if (isset($_SESSION['auth']) && $_SESSION['auth'])
	{
	$data['title'] = "Profili";	
	$data['emri'] = $_SESSION['emri'];
	echo $twig->render('profili.html', $data);
	}
else
	{
	$data['title'] = "Gabim";
	$data['raporti'] = "Nuk jeni te regjistruar, identifokuni <a href='login.php'>ketu</a>";
	echo $twig->render('raporti.html', $data);	
	}
?>