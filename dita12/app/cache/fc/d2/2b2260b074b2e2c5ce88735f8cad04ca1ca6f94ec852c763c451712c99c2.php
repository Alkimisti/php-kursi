<?php

/* resetpassword.html */
class __TwigTemplate_fcd22b2260b074b2e2c5ce88735f8cad04ca1ca6f94ec852c763c451712c99c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action=\"/resetpassword\" method=\"post\">
<p>Shenojeni emailin tuaj</p>
<p><input type=\"email\" name=\"email\"></p>
<p><input type=\"submit\" value=\"Dergo\"></p>
</form>
";
    }

    public function getTemplateName()
    {
        return "resetpassword.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
