<?php

/* admin/produktet.html */
class __TwigTemplate_3a604984cf546b4aa5e64391a8d8f2d6f339dd74dde4ac061e038da95b0ab4ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Produktet</h1>
    <p><a class=\"btn btn-default\" href=\"/admin/produkti\">Shto nje produkt te ri</a></p>
     <div>";
        // line 7
        echo (isset($context["links"]) ? $context["links"] : null);
        echo "</div>
    <table class=\"table\">
    ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produktet"]) ? $context["produktet"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 10
            echo "    <tr>
    <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Price", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Cat_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_Name", array()), "html", null, true);
            echo "</td>
    <td><a class='btn btn-success' href='/admin/produkti/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "'>Edito</a></td>
    <td><a class='btn btn-danger' href='/admin/fshijeproduktin/";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "'>Fshije</a></td>    
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "    </table>
   
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/produktet.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 20,  81 => 17,  77 => 16,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  57 => 11,  54 => 10,  50 => 9,  45 => 7,  39 => 3,  36 => 2,  11 => 1,);
    }
}
