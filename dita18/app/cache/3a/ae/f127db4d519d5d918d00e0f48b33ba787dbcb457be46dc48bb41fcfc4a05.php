<?php

/* shporta.html */
class __TwigTemplate_3aaef127db4d519d5d918d00e0f48b33ba787dbcb457be46dc48bb41fcfc4a05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>
";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<h2>Shporta</h2>
<form method=\"post\" action=\"/porosit\" >
<table class='table'>
<tr><th>ID</th>
<th>Emertimi</th>
<th>Cmimi</th>
<th>Sasia</th>
<th>Vlera</th>
</tr>

";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabela"]) ? $context["tabela"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["rr"]) {
            // line 17
            echo "<tr>
\t\t<td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "id", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "proname", array()), "html", null, true);
            echo "</td>
\t\t<td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "cmimi", array()), "html", null, true);
            echo "</td>
\t\t<td><input class=\"sasia\" data-price=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "cmimi", array()), "html", null, true);
            echo "\" type=\"number\" name=\"qty[";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "id", array()), "html", null, true);
            echo "]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "qty", array()), "html", null, true);
            echo "\"></td>
\t\t<td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["rr"], "vlera", array()), "html", null, true);
            echo "</td>
\t\t</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rr'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        
<tr><td>&nbsp;</td>
\t\t<td>&nbsp;</td>
\t\t<td>&nbsp;</td>
\t\t<td>Total:</td>
\t\t<td><strong>";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["shuma"]) ? $context["shuma"] : null), "html", null, true);
        echo "</strong></td>
\t\t</tr>
</table>     
<p>Komenti juaj:</p>   \t
\t<textarea name=\"Comment\"></textarea>
\t<input type=\"submit\" class='btn btn-success' value=\"Porosit\">\t
</form>
<script>
\$(\".sasia\").change(function() {
\t
\tvar s = \$(this).val()
\tvar c = \$(this).data('price')
\tvar vlera = s*c
///
})

</script>
";
    }

    public function getTemplateName()
    {
        return "shporta.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 29,  96 => 24,  87 => 22,  79 => 21,  75 => 20,  71 => 19,  67 => 18,  64 => 17,  60 => 16,  48 => 6,  45 => 5,  40 => 3,  37 => 2,  11 => 1,);
    }
}
