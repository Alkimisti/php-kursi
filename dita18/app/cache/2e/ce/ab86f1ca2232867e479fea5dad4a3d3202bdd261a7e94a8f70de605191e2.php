<?php

/* admin/produktet.html */
class __TwigTemplate_2eceab86f1ca2232867e479fea5dad4a3d3202bdd261a7e94a8f70de605191e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Produktet</h1>
    <p><a class=\"btn btn-default\" href=\"/admin/produkti\">Shto nje produkt te ri</a></p>
     <div>";
        // line 7
        echo (isset($context["links"]) ? $context["links"] : null);
        echo "</div>
     <form method=\"post\" action=\"/admin/fshijeproduktin\">
    <table class=\"table\">
    ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produktet"]) ? $context["produktet"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 11
            echo "    <tr>
    <td><input type=\"checkbox\" name=\"fshij[";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "]\"></td>
    <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Price", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Cat_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_Name", array()), "html", null, true);
            echo "</td>
    <td><a class='btn btn-success' href='/admin/produkti/";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "'>Edito</a></td>
    <td><a class='btn btn-danger' href='/admin/fshijeproduktin/";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "' onclick=\"return confirm('A je i sigurte?')\">Fshije</a></td>    
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    </table>
    <input type=\"submit\" class=\"btn btn-danger\" value=\"Fshij\" onclick=\"return confirm('A je i sigurte?')\">
   </form>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/produktet.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 22,  86 => 19,  82 => 18,  78 => 17,  74 => 16,  70 => 15,  66 => 14,  62 => 13,  58 => 12,  55 => 11,  51 => 10,  45 => 7,  39 => 3,  36 => 2,  11 => 1,);
    }
}
