<?php

/* admin/anetaret.html */
class __TwigTemplate_e5014c1d77fa30e338ef7bfee42bf7a33b516f9e37e7a7894350c8982feb6442 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Lista e anetareve</h1>
<ul>
";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["anetaret"]) ? $context["anetaret"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 8
            echo "\t<li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "ID", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "email", array()), "html", null, true);
            echo ") Level: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "level", array()), "html", null, true);
            echo "</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "</ul>\t
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/anetaret.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 10,  49 => 8,  45 => 7,  39 => 3,  36 => 2,  11 => 1,);
    }
}
