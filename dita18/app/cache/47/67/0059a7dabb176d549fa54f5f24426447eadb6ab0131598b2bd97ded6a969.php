<?php

/* produkti.html */
class __TwigTemplate_47670059a7dabb176d549fa54f5f24426447eadb6ab0131598b2bd97ded6a969 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "<style>
.butoni {
\tbackground-color: black;\t
}
</style>
<script src=\"https://code.jquery.com/jquery-2.1.3.min.js\"></script>
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "<h3>Produkti</h3>
<div class=\"col-md-12\">

     <div class='col-md-12'><h1>";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["Man_Name"]) ? $context["Man_Name"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["Pro_Name"]) ? $context["Pro_Name"] : null), "html", null, true);
        echo "</h1></div>
    <div class='col-md-4'>Kodi</div>
    <div class='col-md-8'>";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["Pro_Code"]) ? $context["Pro_Code"] : null), "html", null, true);
        echo "</div>
    
   
    
    
    <div class='col-md-4'>Pershkrimi</div>
    <div class='col-md-8'>";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["Pro_Description"]) ? $context["Pro_Description"] : null), "html", null, true);
        echo "</div>    
    
    <div class='col-md-4'>Cmimi</div>
    <div class='col-md-8'>";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["Pro_Price"]) ? $context["Pro_Price"] : null), "html", null, true);
        echo " €</div> 
     
 
\t<form action=\"/shto\" method=\"post\" >
    
    <div class='col-md-4'>Sasia</div>
    <div class='col-md-8'>
    <input  type=\"number\" class=\"form-control\" name=\"sasia\" value=\"1\"></div>
    <div class='col-md-4'>Sistemi</div>
    <div class='col-md-8'>
    <select name='sistemi' class=\"form-control\">
    ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["OS"]) ? $context["OS"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 38
            echo "     <option value='";
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "id", array()), "html", null, true);
            echo "'>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["o"], "emri", array()), "html", null, true);
            echo "</option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "    </select>

\t</div>    
    
    
    <input type=\"hidden\" name=\"Pro_ID\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["Pro_ID"]) ? $context["Pro_ID"] : null), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"Pro_Price\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["Pro_Price"]) ? $context["Pro_Price"] : null), "html", null, true);
        echo "\">    
    
    <input type=\"hidden\" name=\"crsf_token\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["crsf_token"]) ? $context["crsf_token"] : null), "html", null, true);
        echo "\">

    </div> 
    
    <div class='col-md-4'></div>
    <div class='col-md-8'>
    
    ";
        // line 55
        if ((isset($context["auth"]) ? $context["auth"] : null)) {
            // line 56
            echo "     <input class=\"btn btn-success\" type=\"submit\" value=\"Bleje\">
     ";
        }
        // line 58
        echo "    </form>   
    </div>     
</div>
";
    }

    public function getTemplateName()
    {
        return "produkti.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 58,  137 => 56,  135 => 55,  125 => 48,  120 => 46,  116 => 45,  109 => 40,  98 => 38,  94 => 37,  80 => 26,  74 => 23,  65 => 17,  58 => 15,  53 => 12,  50 => 11,  40 => 3,  37 => 2,  11 => 1,);
    }
}
