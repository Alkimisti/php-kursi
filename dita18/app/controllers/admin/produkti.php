<?php

if (isset($url[3]) && $url[3]>0)
	{
	$id = intval($url[3]);
	// Update
	
	require("../app/models/produktet_model.php");
	$data['produkti'] = trego_produktin($dbcon, $id);
	$data['produkti']['Kategoria_Select'] = shfaq_kategorite($dbcon, $data['produkti']['Pro_Cat_ID']);	
	
$data['produkti']['Prodhuesit_Select'] = shfaq_prodhuesit($dbcon, $data['produkti']['Pro_Man_ID']);	

	$data['action'] = "/admin/pregjistro/2";	
	}
else
	{
	// Insert
	$kategorite = shfaq_kategorite($dbcon, 0);
	$prodhuesit = shfaq_prodhuesit($dbcon, 0);
	$data['produkti'] = array(
	'Pro_Name' => "",
	'Pro_Code' => "",
	'Pro_Description' => "",
	'Pro_Price' => 0,
	'Pro_Cat_ID' => 0,
	'Kategoria_Select' => $kategorite,
	'Pro_Man_ID' => 0,
	'Prodhuesit_Select' => $prodhuesit);
	$data['action'] = "/admin/pregjistro/1";
	}
	
$data['title'] = "Lista e produkteve";
echo $twig->render('admin/produkti.html', $data);

function shfaq_kategorite($dbcon, $cat=0) {
	
$sql = "SELECT * FROM categories ORDER BY Cat_Name";

$results = mysqli_query($dbcon, $sql);

if (mysqli_num_rows($results) > 0)
	{
	$rezultati = "<select name='Pro_Cat_ID'><option value='0'>Zgjedh...</option>";
	
	while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
			{
			if ($cat == $row['Cat_ID'])
				{
				$rezultati .= "<option value='".$row['Cat_ID']."' selected >".$row['Cat_Name']."</option>";	
				}
			else
				{
				$rezultati .= "<option value='".$row['Cat_ID']."'>".$row['Cat_Name']."</option>";
				}
			}
		$rezultati .= "</select>";
		return  $rezultati;
		}
	else
		{
		return "";	
		}		
}

function shfaq_prodhuesit($dbcon, $pro=0) {
	
$sql = "SELECT * FROM manufacturers ORDER BY Man_Name";

$results = mysqli_query($dbcon, $sql);

if (mysqli_num_rows($results) > 0)
	{
	$rezultati = "<select name='Pro_Man_ID'><option value='0'>Zgjedh...</option>";
	
	while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC))
			{
			if ($pro == $row['Man_ID'])
				{
				$rezultati .= "<option value='".$row['Man_ID']."' selected >".$row['Man_Name']."</option>";	
				}
			else
				{
				$rezultati .= "<option value='".$row['Man_ID']."'>".$row['Man_Name']."</option>";
				}
			}
		$rezultati .= "</select>";
		return  $rezultati;
		}
	else
		{
		return "";	
		}
		
}