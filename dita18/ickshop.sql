-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2015 at 12:30 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ickshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cat_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Cat_ID`),
  KEY `Cat_Name` (`Cat_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Cat_ID`, `Cat_Name`) VALUES
(2, 'Kompjuter'),
(1, 'Laptop'),
(4, 'Printer'),
(3, 'Skener');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `Itm_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Itm_Ord_ID` int(11) NOT NULL,
  `Itm_Pro_ID` int(11) NOT NULL,
  `Itm_Price` decimal(6,2) NOT NULL,
  `Itm_Qty` decimal(6,2) NOT NULL,
  `Itm_Code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Itm_Options` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Itm_ID`),
  KEY `Itm_Ord_ID` (`Itm_Ord_ID`,`Itm_Pro_ID`,`Itm_Price`,`Itm_Qty`,`Itm_Code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`Itm_ID`, `Itm_Ord_ID`, `Itm_Pro_ID`, `Itm_Price`, `Itm_Qty`, `Itm_Code`, `Itm_Options`) VALUES
(1, 5, 2, '999.95', '1.00', '54565858', ''),
(2, 6, 1, '650.15', '4.00', '00021542', ''),
(3, 6, 2, '999.95', '5.00', '54565858', ''),
(4, 7, 1, '650.15', '1.00', '00021542', ''),
(5, 8, 2, '999.95', '3.00', '54565858', ''),
(6, 8, 1, '650.15', '1.00', '00021542', ''),
(7, 9, 2, '999.95', '3.00', '54565858', ''),
(8, 9, 1, '650.15', '1.00', '00021542', ''),
(9, 10, 2, '999.95', '3.00', '54565858', ''),
(10, 10, 1, '650.15', '1.00', '00021542', ''),
(11, 11, 1, '650.15', '1.00', '00021542', ''),
(12, 12, 1, '650.15', '3.00', '00021542', ''),
(13, 13, 1, '650.15', '1.00', '00021542', ''),
(14, 13, 2, '999.95', '1.00', '54565858', '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `Man_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Man_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Man_ID`),
  KEY `Man_Name` (`Man_Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`Man_ID`, `Man_Name`) VALUES
(2, 'Canon'),
(5, 'Dell'),
(1, 'HP'),
(4, 'SONY'),
(3, 'Toshiba');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `Ord_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ord_Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Ord_Usr_ID` int(11) NOT NULL,
  `Ord_Status` int(11) NOT NULL,
  `Ord_Comment` text COLLATE utf8_unicode_ci NOT NULL,
  `Ord_Total` decimal(6,2) NOT NULL,
  PRIMARY KEY (`Ord_ID`),
  KEY `Ord_Date` (`Ord_Date`,`Ord_Usr_ID`,`Ord_Status`,`Ord_Total`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Ord_ID`, `Ord_Date`, `Ord_Usr_ID`, `Ord_Status`, `Ord_Comment`, `Ord_Total`) VALUES
(2, '2015-01-30 18:23:59', 2, 1, 'fdgd gdf g f', '0.00'),
(3, '2015-01-30 18:24:19', 2, 1, 'fdgd gdf g f', '0.00'),
(4, '2015-01-30 18:24:54', 2, 1, '', '0.00'),
(5, '2015-01-30 18:25:40', 2, 1, '', '0.00'),
(6, '2015-01-30 18:38:10', 2, 1, 'kjhk kjh kj', '0.00'),
(7, '2015-01-30 18:41:41', 2, 1, '', '0.00'),
(8, '2015-01-30 18:42:27', 2, 1, 'hkjh', '0.00'),
(9, '2015-01-30 18:43:18', 2, 1, 'hkjh', '0.00'),
(10, '2015-01-30 18:43:24', 2, 1, 'hkjh', '0.00'),
(11, '2015-01-30 18:43:36', 2, 1, '', '0.00'),
(12, '2015-02-02 14:21:21', 2, 1, 'dADa ASD asdASD ASDadA', '0.00'),
(13, '2015-02-02 16:13:35', 2, 1, 'zcdfsadasdf asf asfsad', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `Pro_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Pro_Cat_ID` int(11) NOT NULL,
  `Pro_Man_ID` int(11) NOT NULL,
  `Pro_Code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Pro_Price` decimal(6,2) NOT NULL,
  `Pro_Available` int(11) NOT NULL DEFAULT '1',
  `Pro_Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Pro_ID`),
  KEY `Pro_Cat_ID` (`Pro_Cat_ID`,`Pro_Man_ID`,`Pro_Code`,`Pro_Name`,`Pro_Price`,`Pro_Available`),
  FULLTEXT KEY `Pro_Description` (`Pro_Description`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Pro_ID`, `Pro_Cat_ID`, `Pro_Man_ID`, `Pro_Code`, `Pro_Name`, `Pro_Description`, `Pro_Price`, `Pro_Available`, `Pro_Date`) VALUES
(1, 1, 5, '00021542', 'Inspiron 17R', 'af asdf asf asd fas dfas dfas\r\ndfas\r\ndf asd a sdf asdf asdfas df\r\nasd fasd\r\nfasd\r\nf\r\nasd\r\nfasdf ', '650.15', 1, '2015-02-01 16:11:29'),
(2, 1, 4, '54565858', 'VAIO', 'asdfasdfasd as fd asdf as dfas fas f asdfas\r\ndfas\r\ndfa\r\nsdf\r\nasdf asdf asdf asdf\r\n', '912.00', 1, '2015-02-01 16:11:29'),
(3, 1, 3, '54545', 'Satellite XP987', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '1250.00', 1, '2015-02-02 16:11:29'),
(4, 1, 3, '54545', 'Satellite XP9872', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '986.00', 1, '2015-02-02 16:11:29'),
(5, 1, 3, '545453', 'Satellite XP98723', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '1050.00', 1, '2015-02-02 16:11:29'),
(6, 1, 4, '54565858', 'VAIO3', 'asdfasdfasd as fd asdf as dfas fas f asdfas\r\ndfas\r\ndfa\r\nsdf\r\nasdf asdf asdf asdf\r\n', '957.00', 1, '2015-02-03 16:11:29'),
(7, 1, 4, '54565858', 'VAIOc', 'asdfasdfasd as fd asdf as dfas fas f asdfas\r\ndfas\r\ndfa\r\nsdf\r\nasdf asdf asdf asdf\r\n', '1805.00', 1, '2015-02-04 16:11:29'),
(8, 1, 3, '54545', 'Satellite XP9873', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '569.00', 1, '2015-02-04 16:11:29'),
(9, 1, 5, '00021542', 'Latitude 433', 'af asdf asf asd fas dfas dfas\r\ndfas\r\ndf asd a sdf asdf asdfas df\r\nasd fasd\r\nfasd\r\nf\r\nasd\r\nfasdf ', '300.00', 1, '2015-02-04 16:11:29'),
(10, 1, 3, '54545', 'Satellite XP9872xxxx', 'asdfasdf sadf asdf \r\nasdf a\r\nsdfa\r\nsf\r\nads\r\nf\r\n', '457.00', 1, '2015-02-04 16:11:29'),
(11, 1, 5, '00021542', 'Inspiron 17Rxxxx', 'af asdf asf asd fas dfas dfas\r\ndfas\r\ndf asd a sdf asdf asdfas df\r\nasd fasd\r\nfasd\r\nf\r\nasd\r\nfasdf ', '852.00', 1, '2015-02-04 16:11:29'),
(12, 1, 5, '00021542dd', 'Inspiron 17Rddd', 'af asdf asf asd fas dfas dfas\r\ndfas\r\ndf asd a sdf asdf asdfas df\r\nasd fasd\r\nfasd\r\nf\r\nasd\r\nfasdf ', '951.00', 1, '2015-02-04 16:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `sistemi`
--

CREATE TABLE IF NOT EXISTS `sistemi` (
  `Sis_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sis_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Sis_Price` decimal(6,2) NOT NULL,
  PRIMARY KEY (`Sis_ID`),
  KEY `SIs_Price` (`Sis_Price`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sistemi`
--

INSERT INTO `sistemi` (`Sis_ID`, `Sis_Name`, `Sis_Price`) VALUES
(2, 'Windows 10', '100.00'),
(3, 'Ubuntu 14', '0.00'),
(4, 'MacOS', '150.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `Usr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usr_Email` varchar(50) NOT NULL,
  `Usr_Password` varchar(40) NOT NULL,
  `Usr_Name` varchar(20) NOT NULL,
  `Usr_Level` int(11) NOT NULL DEFAULT '0',
  `Usr_Salt` int(11) NOT NULL,
  `Usr_Random` int(11) NOT NULL,
  PRIMARY KEY (`Usr_ID`),
  UNIQUE KEY `Usr_Email` (`Usr_Email`),
  KEY `Usr_Password` (`Usr_Password`),
  KEY `Usr_Level` (`Usr_Level`),
  KEY `Usr_Salt` (`Usr_Salt`),
  KEY `Usr_Random` (`Usr_Random`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Usr_ID`, `Usr_Email`, `Usr_Password`, `Usr_Name`, `Usr_Level`, `Usr_Salt`, `Usr_Random`) VALUES
(1, 'tahir.hoxha@gmail.com', '9aa824d6e528fd945f96cb82c20e2701e8c75abc', 'Tahir Hoxha', 2, 7472869, 8263617),
(2, 'korab@korab.com', '98c5a3012cfb0a19ec1da2ba462cbecc6bceb7fd', 'Korab', 2, 7538787, 0),
(15, 'zanfinagashi@gmail.com', '0ae59a1a1871a9f879cd49361291baf250d3f363', 'Zanfina Gashi', 1, 1970092, 0),
(8, 'alkimisti@live.com', '8b33e025bf2d6e48717c63ae5bf0c182e950dbc5', 'Alkimisti', 0, 3622161, 0),
(23, 'tahirhoxha@gmail.com', '2d77b187f896127ca589274c2af8281e059d3bc8', 'Tahir', 1, 2446899, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
