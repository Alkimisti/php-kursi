<?php
$data['raporti'] = "";

if (isset($url[3]) && $url[3] > 0) {
    $op = intval($url[3]);
}

$pro_id = mysqli_real_escape_string($dbcon, intval($_POST['Pro_ID']));
$cat_id = mysqli_real_escape_string($dbcon, intval($_POST['Pro_Cat_ID']));
$man_id = mysqli_real_escape_string($dbcon, intval($_POST['Pro_Man_ID']));
$pro_name = mysqli_real_escape_string($dbcon, filter_var($_POST['Pro_Name'], FILTER_SANITIZE_STRING));
$pro_code = mysqli_real_escape_string($dbcon,filter_var($_POST['Pro_Code'], FILTER_SANITIZE_STRING));
$pro_description = mysqli_real_escape_string($dbcon,filter_var($_POST['Pro_Description'], FILTER_SANITIZE_STRING));
$pro_price = mysqli_real_escape_string($dbcon,floatval($_POST['Pro_Price']));

// validimi

if ($op == 1) {
    // insert
    $sql = "INSERT INTO products (Pro_Cat_ID, Pro_Man_ID, Pro_Name, Pro_Code, Pro_Description, Pro_Price) 
	VALUES ($cat_id, $man_id, '$pro_name', '$pro_code', '$pro_description', $pro_price)";

    $rez = mysqli_query($dbcon, $sql);
    $pro_id = mysqli_insert_id($dbcon);
    $data['raporti'] = "Te dhenat u regjistruan me sukses!";
} elseif ($op == 2) {
    // update
    $sql = "UPDATE products 
	SET Pro_Cat_ID = $cat_id,
	Pro_Man_ID = $man_id,
	Pro_Name = '$pro_name',
	Pro_Code = '$pro_code',
	Pro_Description = '$pro_description',
	Pro_Price = $pro_price
	WHERE Pro_ID = $pro_id
	LIMIT 1";

    // $pro_id
    $rez = mysqli_query($dbcon, $sql);
    $data['raporti'] .= "<p>Te dhenat u korigjuan me sukses!</p>";
} else {
    $data['raporti'] .= "<p>Veprim i panjohur!</p>";
}

$fajllat = $_FILES['fajlli'];
$emrat = $_FILES['fajlli']['name'];
$mimetipi = $_FILES['fajlli']['type'];
$madhesia = $_FILES['fajlli']['size'];
$emri_perkohshem = $_FILES['fajlli']['tmp_name'];
$gabimet = $_FILES['fajlli']['error'];


foreach ($gabimet as $key => $value) {
    if ($value == 0) {
        if ($madhesia[$key] <= 10000000) {
            if ($mimetipi[$key] == "image/jpeg") {
                $filename = "p-" . $pro_id . "-" . $key . ".jpg";
                if (move_uploaded_file($emri_perkohshem[$key], "products/" . $filename)) {
                    ImageResize(600, $filename);
					ImageResize(200, $filename, "thumb_");
					$data['raporti'] .= "<p>Foto " . $key . "  u procesua me sukses</p>";

                } else {
                    $data['raporti'] .= "<p>Foto " . $key . " deshtoi</p>";
                }
            } else {
                $data['raporti'] .= "<p>Foto " . $key . " nuk eshte JPEG</p>";
            }
        } else {
            $data['raporti'] .= "<p>Foto " . $key . " teper e madhe</p>";
        }
    } else {
        switch ($value) {
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
            case UPLOAD_ERR_PARTIAL:
            case UPLOAD_ERR_NO_TMP_DIR:
            case UPLOAD_ERR_CANT_WRITE:
            case UPLOAD_ERR_EXTENSION:
                $data['raporti'] .= "<p>Foto " . $key . " ka gabime</p>";
                break;
            case UPLOAD_ERR_NO_FILE:
                //
                break;
            default:
                //	
        }

    }
}

$data['title'] = "Regjistrimi/ndryshimi i produkteve";
echo $twig->render('raporti.html', $data);


function ImageResize($max, $img_name, $prefix="")
{
    /* Get original file size */
    list($w, $h) = getimagesize("products/" . $img_name);

	
	if ($w > $h) // landscape
		{
			$a = $max;
			$b = ceil($h / ($w / $max));
		}
	elseif ($w < $h) // portrait
		{
			$b = $max;
			$a = ceil($w / ($h / $max));
		}
	else //square
		{
			$a = $max; $b = $max;	
		}
	

	
    $path = "products/" . $prefix . $img_name;


    /* Save image */

    /* Get binary data from image */
    $imgString = file_get_contents("products/" . $img_name);
    /* create image from string */
    $image = imagecreatefromstring($imgString);
    $tmp = imagecreatetruecolor($a, $b);
    imagecopyresampled($tmp, $image, 0, 0, 0, 0,$a, $b, $w, $h);
    imagejpeg($tmp, $path, 60);

    imagedestroy($image);
    imagedestroy($tmp);
}