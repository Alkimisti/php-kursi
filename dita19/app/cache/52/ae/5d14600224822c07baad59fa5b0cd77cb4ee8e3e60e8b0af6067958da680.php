<?php

/* produktet.html */
class __TwigTemplate_52ae5d14600224822c07baad59fa5b0cd77cb4ee8e3e60e8b0af6067958da680 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Produktet</h1>
     <div>";
        // line 6
        echo (isset($context["links"]) ? $context["links"] : null);
        echo "</div>
    <table class=\"table\">
    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produktet"]) ? $context["produktet"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 9
            echo "    <tr>
    <td>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_Price", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Cat_Name", array()), "html", null, true);
            echo "</td>
    <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_Name", array()), "html", null, true);
            echo "</td>
    <td><a class='btn btn-success' href='/produkti/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "'>Hape</a></td>
        <td><a data-id=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Pro_ID", array()), "html", null, true);
            echo "\" class='btn btn-default ajaxlink' href='#'>AJAX</a></td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    </table>
   
\t</div>
</div>
<div id=\"detajet\"></div>
<script>
\$(document).ready(function() {
\t\$(\".ajaxlink\").click(function(e) {
\t\te.preventDefault();
\t\t
\t\tvar id = \$(this).data('id')
\t\t
\t\t \$.ajax({
\t\t\t method: \"get\",
\t\t\t url: \"/detajet/\" + id,
\t\t\t success: function(result){
        \$(\"#detajet\").html(result);
    }});
\t
\t\t
\t});
\t
});

</script>
";
    }

    public function getTemplateName()
    {
        return "produktet.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 19,  80 => 16,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  60 => 11,  56 => 10,  53 => 9,  49 => 8,  44 => 6,  39 => 3,  36 => 2,  11 => 1,);
    }
}
