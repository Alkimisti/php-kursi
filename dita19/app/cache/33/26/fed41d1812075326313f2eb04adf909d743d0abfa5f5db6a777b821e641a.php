<?php

/* login.html */
class __TwigTemplate_3326fed41d1812075326313f2eb04adf909d743d0abfa5f5db6a777b821e641a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action=\"/check\" method=\"post\">
<p>Username: <input type=\"text\" name=\"user\"></p>
<p>Password: <input type=\"password\" name = \"pass\" ></p>

<p><input class=\"btn btn-success\" type=\"submit\" value=\"Login\"> 
</form>
<p><a href=\"/resetpassword\">E kam harruar passwordin</a></p>
";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
