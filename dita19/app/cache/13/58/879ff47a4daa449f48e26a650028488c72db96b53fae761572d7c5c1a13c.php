<?php

/* produkti.html */
class __TwigTemplate_1358879ff47a4daa449f48e26a650028488c72db96b53fae761572d7c5c1a13c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "<style>
.butoni {
\tbackground-color: black;\t
}
</style>

";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "<h3>Produkti</h3>
<div class=\"col-md-12\">

     <div class='col-md-12'><h1>";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["Man_Name"]) ? $context["Man_Name"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["Pro_Name"]) ? $context["Pro_Name"] : null), "html", null, true);
        echo "</h1></div>
     
     
     <div class='col-md-12'>
     
     ";
        // line 20
        if ((isset($context["imazhi"]) ? $context["imazhi"] : null)) {
            // line 21
            echo "     \t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["imazhi"]) ? $context["imazhi"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["im"]) {
                // line 22
                echo "        <div class='col-md-2'>
        \t<a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["im"], "full", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["im"], "thumb", array()), "html", null, true);
                echo "\" ></a>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['im'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "     ";
        }
        // line 27
        echo "     
     </div>
     
    <div class='col-md-4'>Kodi</div>
    <div class='col-md-8'>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["Pro_Code"]) ? $context["Pro_Code"] : null), "html", null, true);
        echo "</div>
    
   
    
    
    <div class='col-md-4'>Pershkrimi</div>
    <div class='col-md-8'>";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["Pro_Description"]) ? $context["Pro_Description"] : null), "html", null, true);
        echo "</div>    
    
    <div class='col-md-4'>Cmimi</div>
    <div class='col-md-8'>";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["Pro_Price"]) ? $context["Pro_Price"] : null), "html", null, true);
        echo " €</div> 
     
 
\t<form action=\"/shto\" method=\"post\" >
    
    <div class='col-md-4'>Sasia</div>
    <div class='col-md-8'>
    <input  type=\"number\" class=\"form-control\" name=\"sasia\" value=\"1\"></div>
     
    
    
    <input type=\"hidden\" name=\"Pro_ID\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["Pro_ID"]) ? $context["Pro_ID"] : null), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"Pro_Price\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["Pro_Price"]) ? $context["Pro_Price"] : null), "html", null, true);
        echo "\">    
    
    <input type=\"hidden\" name=\"crsf_token\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, (isset($context["crsf_token"]) ? $context["crsf_token"] : null), "html", null, true);
        echo "\">

    </div> 
    
    <div class='col-md-4'></div>
    <div class='col-md-8'>
    
    ";
        // line 61
        if ((isset($context["auth"]) ? $context["auth"] : null)) {
            // line 62
            echo "     <input class=\"btn btn-success\" type=\"submit\" value=\"Bleje\">
     ";
        }
        // line 64
        echo "    </form>   
    </div>     
</div>
";
    }

    public function getTemplateName()
    {
        return "produkti.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 64,  148 => 62,  146 => 61,  136 => 54,  131 => 52,  127 => 51,  113 => 40,  107 => 37,  98 => 31,  92 => 27,  89 => 26,  78 => 23,  75 => 22,  70 => 21,  68 => 20,  58 => 15,  53 => 12,  50 => 11,  40 => 3,  37 => 2,  11 => 1,);
    }
}
