<?php

/* login.html */
class __TwigTemplate_0840980734d65c32142c25e0cfbcc81fc287c310d78380b5080c088149192b54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<form action=\"/check\" method=\"post\">
<p>Username: <input type=\"text\" name=\"user\"></p>
<p>Password: <input type=\"password\" name = \"pass\" ></p>

<p><input class=\"btn btn-success\" type=\"submit\" value=\"Login\"> 
</form>
<p><a href=\"/resetpassword\">E kam harruar passwordin</a></p>
";
    }

    public function getTemplateName()
    {
        return "login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
