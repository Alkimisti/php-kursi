<?php

/* admin/produkti.html */
class __TwigTemplate_ef6d1a8bdd0d6cf57af9c10617249ce2e45332f8c4ca98602680944e44fd2da4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Produkti</h1>
    
    <form action=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
    <input type=\"hidden\" name=\"Pro_ID\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_ID", array()), "html", null, true);
        echo "\">
    
    <div class=\"col-md-3\">
    Kategoria e produktit
    </div>
    <div class=\"col-md-9\">
    ";
        // line 14
        echo $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Kategoria_Select", array());
        echo "
    </div> 
    
    <div class=\"col-md-3\">
    Prodhuesi
    </div>
    <div class=\"col-md-9\">
    ";
        // line 21
        echo $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Prodhuesit_Select", array());
        echo "
    </div>       
    
    
    <div class=\"col-md-3\">
    Emri i produktit
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\"  type=\"text\" name=\"Pro_Name\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Name", array()), "html", null, true);
        echo "\" >
    </div>  
    
    <div class=\"col-md-3\">
    Kodi i produktit
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\" type=\"text\" name=\"Pro_Code\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Code", array()), "html", null, true);
        echo "\" >
    </div>        
    
    
    <div class=\"col-md-3\">
    Pershkrimi i produktit
    </div>
    <div class=\"col-md-9\">
    <textarea rows=\"8\" class=\"form-control\" name=\"Pro_Description\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Description", array()), "html", null, true);
        echo "</textarea>
    </div>        
     
     
         <div class=\"col-md-3\">
    Cmimi
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\" type=\"text\" name=\"Pro_Price\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Price", array()), "html", null, true);
        echo "\" >
    </div> 
    
    <div class=\"col-md-9\">
    <input type=\"file\" name=\"fajlli[]\">
    <input type=\"file\" name=\"fajlli[]\">
    <input type=\"file\" name=\"fajlli[]\">
    </div>           
    <input type=\"submit\" class=\"btn btn-success\" value=\"Regjistro\">
    </form>
</div>
</div>   
";
    }

    public function getTemplateName()
    {
        return "admin/produkti.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 52,  100 => 44,  89 => 36,  79 => 29,  68 => 21,  58 => 14,  49 => 8,  45 => 7,  39 => 3,  36 => 2,  11 => 1,);
    }
}
