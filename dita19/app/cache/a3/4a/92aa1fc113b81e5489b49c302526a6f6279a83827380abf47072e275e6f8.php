<?php

/* kerkimiav.html */
class __TwigTemplate_a34a92aa1fc113b81e5489b49c302526a6f6279a83827380abf47072e275e6f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
  <div class=\"col-md-12\">
    <h1>Kërkim i avancuar</h1>
    <form method=\"post\" action=\"/rezultatet2\">
      <table class=\"table\">
        <tr>
          <td>Emri i produktit</td>
          <td><input class=\"form-control\" type=\"text\" name=\"emri\" id=\"emri\"></td>
        </tr>
        <tr>
          <td>Çmimi</td>
          <td> Prej
            <input  type=\"text\" name=\"cmimi1\" id=\"cmimi1\" value=\"0\">
            Deri
            <input  type=\"text\" name=\"cmimi2\" id=\"cmimi2\" value=\"0\"></td>
        </tr>
        <tr>
          <td>Kategoria</td>
          <td><select  class=\"form-control\" name=\"kategoria\">
          <option value=\"0\" selected>Të gjitha</option>
              
              
          ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["kategorite"]) ? $context["kategorite"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            // line 26
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["k"], "Cat_ID", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["k"], "Cat_Name", array()), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "          
            
            </select></td>
        </tr>
        <tr>
          <td>Prodhuesi</td>
          <td><select name=\"prodhuesi[]\" size=\"10\" multiple class=\"form-control\">
              <option value=\"0\" selected>Të gjithë</option>
              
              
          ";
        // line 38
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prodhuesit"]) ? $context["prodhuesit"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 39
            echo "          <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_ID", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "Man_Name", array()), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "          
            
            </select></td>
        </tr>
        <tr>
          <td>Renditja</td>
          <td><select name=\"renditja\" class=\"form-control\">
              <option value=\"1\">Prej më të lirave</option>
              <option value=\"2\">Prej më të shtrenjtave</option>
              <option value=\"3\">Prej më të rejave</option>
              <option value=\"4\">Prej më të vjetrave</option>
            </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type=\"submit\" class=\"btn btn-success\" id=\"submit\" value=\"Kërko\"></td>
        </tr>
      </table>
    </form>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "kerkimiav.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 41,  94 => 39,  90 => 38,  78 => 28,  67 => 26,  63 => 25,  39 => 3,  36 => 2,  11 => 1,);
    }
}
