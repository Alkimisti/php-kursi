<?php

/* profili.html */
class __TwigTemplate_871c58a7d57cffb3d4f96254e3c8f4dff7ac935b4a0791dde420fdb9fb1b7d36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h1>Profili</h3>
<h3>Ky eshte profili i ";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["emri"]) ? $context["emri"] : null), "html", null, true);
        echo "</h3>
";
    }

    public function getTemplateName()
    {
        return "profili.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 4,  39 => 3,  36 => 2,  11 => 1,);
    }
}
