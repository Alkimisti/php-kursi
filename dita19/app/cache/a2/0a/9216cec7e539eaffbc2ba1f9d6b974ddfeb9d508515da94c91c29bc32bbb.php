<?php

/* layout.html */
class __TwigTemplate_a20a9216cec7e539eaffbc2ba1f9d6b974ddfeb9d508515da94c91c29bc32bbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css\" rel=\"stylesheet\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/stili.css\">
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script> 
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js\"></script>
";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 12
        echo "</head>

<body>
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-12\">
      <h1>Online Shop</h1>
      
      
      
      
      
      
      <nav class=\"navbar navbar-default\">
        <div class=\"container-fluid\"> 
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\"> <span class=\"sr-only\">Toggle navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> </button>
            <a class=\"navbar-brand\" href=\"/\">Home</a> </div>
          
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
              <li><a href='/produktet'>Produktet</a></li>
              <li><a href='/kontakti'>Kontakti</a></li>
              <li><a href='/kerkimiav'>Kërkim i avancuar</a></li>
              ";
        // line 38
        if ((isset($context["auth"]) ? $context["auth"] : null)) {
            // line 39
            echo "              <li><a href='/shporta'>Shporta</a>
              <li><a href='/zbraze'>Zbraze</a>
              <li><a href='/profili'>Profili</a>
              <li><a href='/logout'>Logout</a> ";
        } else {
            // line 43
            echo "              <li><a href='/login'>Login</a></li>
              <li><a href='/signup'>Regjistrohu</a></li>
              ";
        }
        // line 46
        echo "              
              ";
        // line 47
        if ((isset($context["admin"]) ? $context["admin"] : null)) {
            // line 48
            echo "              <li class=\"dropdown\"> <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">Admin <span class=\"caret\"></span></a>
                <ul class=\"dropdown-menu\" role=\"menu\">
                  <li><a href=\"/admin/produktet\">Produktet</a></li>
                  <li><a href=\"/admin/produkti\">Shto produkt</a></li>
                  <li><a href=\"#\">Porosite</a></li>
                  <li><a href=\"#\">Anetaret</a></li>
                </ul>
              </li>
              ";
        }
        // line 57
        echo "            </ul>
            <form method=\"GET\" action=\"/kerkimi\" class=\"navbar-form navbar-left\" role=\"search\">
              <div class=\"form-group\">
                <input type=\"text\" class=\"form-control\" placeholder=\"Shëno frazën\" name=\"fraza\" required>
              </div>
              <button type=\"submit\" class=\"btn btn-default\">Kërko</button>
            </form>
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <!-- /.container-fluid --> 
      </nav>
      ";
        // line 70
        $this->displayBlock('content', $context, $blocks);
        echo " </div>
  </div>
</div>

</body>
</html>
";
    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 70,  120 => 10,  109 => 70,  94 => 57,  83 => 48,  81 => 47,  78 => 46,  73 => 43,  67 => 39,  65 => 38,  37 => 12,  35 => 10,  27 => 5,  21 => 1,);
    }
}
