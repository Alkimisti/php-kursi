<?php

/* layout.html */
class __TwigTemplate_d82bf6f422d91dfce5b3b25b7d6fb63edc653cd99cf39ec484fa923eaae06626 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css\" rel=\"stylesheet\"><link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/stili.css\">

";
        // line 8
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "</head>

<body>
<div class=\"container\">
<div class=\"row\">
<div class=\"col-md-12\">
<h1>Online Shop</h1>



<nav class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a class=\"navbar-brand\" href=\"/\">Home</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
       

<li><a href='/produktet'>Produktet</a></li>
<li><a href='/kontakti'>Kontakti</a></li>
<li><a href='/kerkimiav'>Kërkim i avancuar</a></li> 
";
        // line 41
        if ((isset($context["auth"]) ? $context["auth"] : null)) {
            // line 42
            echo "\t<li><a href='/shporta'>Shporta</a>
    <li><a href='/zbraze'>Zbraze</a>\t\t
\t<li><a href='/profili'>Profili</a>
\t<li><a href='/logout'>Logout</a>
";
        } else {
            // line 47
            echo "\t<li><a href='/login'>Login</a></li>
\t<li><a href='/signup'>Regjistrohu</a></li>
\t   
";
        }
        // line 51
        echo "   
      </ul>
    <form method=\"GET\" action=\"/kerkimi\" class=\"navbar-form navbar-left\" role=\"search\">
        <div class=\"form-group\">
          <input type=\"text\" class=\"form-control\" placeholder=\"Shëno frazën\" name=\"fraza\" required>
        </div>
        <button type=\"submit\" class=\"btn btn-default\">Kërko</button>
      </form>
    </div><!-- /.navbar-collapse -->
          

  </div><!-- /.container-fluid -->
</nav>
";
        // line 64
        $this->displayBlock('content', $context, $blocks);
        // line 65
        echo "
</div>
</div>
</div>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js\">
</body>
</html>";
    }

    // line 8
    public function block_head($context, array $blocks = array())
    {
    }

    // line 64
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 64,  110 => 8,  100 => 65,  98 => 64,  83 => 51,  77 => 47,  70 => 42,  68 => 41,  35 => 10,  33 => 8,  27 => 5,  21 => 1,);
    }
}
