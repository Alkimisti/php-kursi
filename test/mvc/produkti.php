<?php
include("modelet.php");
include("func.php");

$id = (isset($_GET['id'])) ? intval($_GET['id']) : 0;
$result = getproduct($id);
$data = array();
if (!empty($result))
   {
   $data['cmimi'] = $result['cmimi'];
   $data['tvsh'] = $data['cmimi']*16/100;
   $data['cmimitvsh'] = $data['cmimi']+$data['tvsh'];
   $data['emri'] = $result['emri'];
   $data['kodi'] = $result['kodi'];
   template("produkti", $data); 
        
   }
else
   {
   $data['raporti'] = "Nuk ekziston ky produkt";
   template("raporti", $data); 
   }
