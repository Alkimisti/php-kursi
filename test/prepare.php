<?php 
$link = mysqli_connect("localhost", "root", "", "ickshop"); 

if (!$link)
{
    $error = mysqli_connect_error();
    $errno = mysqli_connect_errno();
    print "$errno: $error\n";
    exit();
}

$query = "SELECT * FROM products WHERE Pro_Cat_ID=?";

$stmt = mysqli_stmt_init($link);
if(!mysqli_stmt_prepare($stmt, $query))
{
    print "Failed to prepare statement\n";
}
else
{
	$category = "1";
    mysqli_stmt_bind_param($stmt, "i", $category);
	// i - integer
	// d - double
 	// s - string
	// b - blob
	

        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
		
		echo "<table>";
        while ($row = mysqli_fetch_assoc($result))
        {

            echo "<tr>
			<td>".$row['Pro_ID']."</td>
			<td>".$row['Pro_Name']."</td></tr>";

    	}
		echo "</table>";
}
mysqli_stmt_close($stmt);
mysqli_close($link);
?>
