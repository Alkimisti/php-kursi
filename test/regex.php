<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="js/jquery-2.1.3.min.js"></script>
</head>

<body>
<form id="regexform">
<select id="samples">
<option value="">Zgjedh</option>
<option value="/\s+/">Space, 1 e me shume</option>
</select>
  <p>
    <label for="pattern">Pattern:<br>
    </label>
    <input name="pattern" type="text" id="pattern" size="80">
  </p>
    <p>
    <label for="regex">Replacement:<br>
    </label>
    <input name="replacement" type="text" id="replacement" size="80">
  </p>
  <p>

    <select name="method" id="method">
    <option value='1'>preg_match()</option>
    <option value='2'>preg_match_all()</option>
    <option value='3'>preg_replace()</option>
    </select>
  </p>
  <p>
    <textarea name="text" cols="80" rows="5" id="text"></textarea>
    </p>
 <p>
<input type="submit" name="submit" id="submit" value="Submit">
  </p>

  <pre><div id="result"></div></pre>
  <p>
</form>
<script>
$("#samples").change(function() {
	$("#pattern").val($("#samples").val());
});

$("#regexform").submit(function(e) {
	
	e.preventDefault();
	var pattern = $("#pattern").val();
	var text = $("#text").val();
	var method = $("#method").val();
	var replacement = $("#replacement").val();
	
 $.ajax({
            data: { 'p' : pattern, 't' : text, 'm' : method, 'r' : replacement },
            type: 'POST',
            url: '/regexcheck.php', 
			success: function(res) {
          $("#result").html(res)

		
    }
        })
	
});

</script>
</body>
</html>