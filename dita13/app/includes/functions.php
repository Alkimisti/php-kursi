<?php
function dergo($to, $subject, $message, $from="")
	{
	//error_reporting(0);
	global $config;
	
	$from = (empty($from))? $config['email_username'] : $from;
	
	// Create the Transport
	$transport = Swift_SmtpTransport::newInstance($config['email_smtp'], $config['email_port'], $config['email_secure'])
  ->setUsername($config['email_username'])
  ->setPassword($config['email_password'])
  ;

	// Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($transport);

	// Create a message
	$message = Swift_Message::newInstance($subject)
	  ->setFrom(array($from))
	  ->setTo(array($to))
	  ->setCc(array($from))
	  ->setBody($message)
	  ->addPart($message, 'text/html');

	// Send the message

	if(!$mailer->send($message))
		{
		return false;	
		}
	else
		{
		return true;	
		}
		
	}
	
