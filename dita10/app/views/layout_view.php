<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=isset($title)?$title:"Untitled";?></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="assets/css/stili.css">
</head>

<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>Online Shop</h1>



<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Home</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       

<?php 
echo "<li><a href='/produktet'>Produktet</a></li>";
echo "<li><a href='/kontakti'>Kontakti</a></li>
";

if (isset($_SESSION['auth']) && $_SESSION['auth'])
	{
	echo "<li><a href='/shporta'>Shporta</a></li>";		
	echo "<li><a href='/profili'>Profili</a></li>";
	echo "<li><a href='/logout'>Logout</a></li>";
	}
else
	{
	echo "<li><a href='/login'>Login</a></li>";
	echo "<li><a href='/signup'>Regjistrohu</a></li>";
	}
?>

        
      </ul>
    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php
$template = "../app/views/".$view."_view.php";
if (file_exists($template))
	{
	include($template);
	}
else
	{
		echo "<p>Nuk ekziston kjo faqe!</p>";
	}

?>
</div>
</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js">
</body>
</html>