<?php if ( ! defined('THIRRJA')) exit('Nuk keni qasje direkte'); ?>

<?php
$shporta = $_SESSION['shporta'];
$tabela = $_SESSION['tabela'];

$to = $_SESSION['email'];
$subject = "Konfirmi i porosise ne ickshop";
$message = "<h1>Konfirmimi i porosise</h1>
<p>I nderuari".$_SESSION['emri'].",</p>
<p>Kemi pranuar nje porosi ne emrin tuaj ne webfaqen www.ickshop.com</p><p>Nese kjo porosi nuk eshte bere nga ju, luteni te na lajmeroni.</p>.
<h2>Permbajtja e porosise</h2>";

$message .= $tabela;

$message .= "<p>Ju falenderojme, </p><p>Stafi i ickshop.</p>";

if (!dergo($to, $subject, $message))
	{
		$data['raporti'] = "Emaili deshtoi";
	}
else
	{
		$data['raporti'] = "Emaili u dergua";
	}

$data['title'] = "Porosia";
$data['view'] = "raporti";