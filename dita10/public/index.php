<?php
session_start();

error_reporting(E_ALL);

if (!isset($_SESSION['shporta']))
	{
	$_SESSION['shporta'] = array();	
	}

define("THIRRJA", 1);

$path = '..\app\libraries\PHPMailer';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once("../app/config/db.php");
require_once("../app/includes/functions.php");
$data = array();
$url = explode("/", $_SERVER['PATH_INFO']);

// Path i kontrollereve
$ctp = "../app/controllers/";
$cnp = "../app/config/";
$vwp = "../app/views/";

if (!empty($url[1]))
	{
	$cmd = filter_var($url[1], FILTER_SANITIZE_STRING);
	
	
	switch ($cmd)
		{
			case 'login':
				require $ctp.'login.php';
				break;
			case 'check':
				require $ctp.'check.php';
				break;
			case 'produktet':
				require $ctp.'produktet.php';
				break;						
			case 'produkti':
				require $ctp.'produkti.php';
				break;
			case 'kontakti':
				require $ctp.'kontakti.php';
				break;
			case 'signup':
				require $ctp.'regjistrimi.php';
				break;		
			default:
				// per auth = true
				if (isset($_SESSION['auth']) && $_SESSION['auth'])
					{
					switch ($cmd)
						{
						case 'shporta':
							require $ctp.'shporta.php';
							break;	
						case 'shto':
							require $ctp.'shto-ne-shporte.php';
							break;																						
						case 'porosit':
							require $ctp.'porosit.php';	
							break;													
						case 'profili':
							require $ctp.'profili.php';
							break;
						case 'logout':
							require $ctp.'logout.php';
							break;
						default:
							//
							if ($_SESSION['level'] == 2)
								{
									switch ($cmd)
										{
										case "anetaret":
										include $ctp.'admin/anetaret.php';	
										}
								}
							else
								{
								$data['raporti'] = "Nuk ekziston kjo faqe!";	
								$data['view'] = "raporti";	
								}
						}
					}
				else
					{
					$data['raporti'] = "Nuk jeni të autorizar!";	
					$data['view'] = "raporti";
					}
		}
	}
else
	{
	include $ctp.'home.php';	
	}

// Mbyllja e koneksionit pas perfundimit te punes se kontrollereve
mysqli_close($dbcon);
	
// konvertimi i data array ne variabla te rendomta
extract($data);
unset($data);

// thirrja e template kryesor
include($vwp.'layout_view.php');