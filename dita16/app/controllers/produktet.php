<?php
include('produktet_model.php');
// Interpretimi i URL per pagination
$per_page = 5;
$page = ($cmd2==0) ?  1: intval($cmd2); 
$from = ($page-1)*$per_page;


// Gjenerimi i linkave per pagination
$totali = total_produkte($dbcon);	

$pages = ceil($totali/$per_page);

$data['links'] = "<nav><ul class='pagination'>";
for ($i = 1; $i <= $pages; $i++)
	{
		if ($i == $page)
			{
			$data['links'] .= "<li class='active'><a href='/produktet/".$i."'>".$i."</a></li>";
			}
		else
			{
			$data['links'] .= "<li><a href='/produktet/".$i."'>".$i."</a></li>";
			}	
	}
$data['links'] .= "</ul></nav>";	



$data['produktet'] = trego_produktet($dbcon, $from, $per_page);
if (!empty($data['produktet'])) {
	
		
	
	
		$data['title'] = "Lista e produkteve";
		echo $twig->render('produktet.html', $data);
	}
else
	{
	$data['raporti'] = "Nuk ka shenime";
	echo $twig->render('raporti.html', $data);
	}
