<?php
include('kerkimi_model.php');

$fraza = filter_var($_GET['fraza'], FILTER_SANITIZE_STRING);




// Interpretimi i URL per pagination
$per_page = 5;
$page = ($cmd2==0) ?  1: intval($cmd2); 
$from = ($page-1)*$per_page;


// Gjenerimi i linkave per pagination
$totali = total_produkte_kerkuara($dbcon, $fraza);	

$pages = ceil($totali/$per_page);
$data['links'] = "";
for ($i = 1; $i <= $pages; $i++)
	{
		if ($i == $page)
			{
			$data['links'] .= "<strong>".$i."</strong> | ";
			}
		else
			{
			$data['links'] .= "<a href='/kerkimi/".$i."?fraza=".$fraza."'>".$i."</a> | ";
			}
		
	
	}



$data['produktet'] = kerko_produktet($dbcon, $fraza, $from, $per_page);
if (!empty($data['produktet'])) {
		$data['title'] = "Lista e produkteve";
		echo $twig->render('kerkimi.html', $data);
	}
else
	{
	$data['raporti'] = "Nuk ka shenime";
	echo $twig->render('raporti.html', $data);
	}
