<?php

/* admin/produkti.html */
class __TwigTemplate_8c534cd23949f87225feb148702e22b661e258e3d2f9af9b3cc7b87c8c49a2e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t<h1>Produkti</h1>
    
    <form >
    <div class=\"col-md-3\">
    Emri i produktit
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\"  type=\"text\" name=\"Pro_Name\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Name", array()), "html", null, true);
        echo "\" >
    </div>  
    
    <div class=\"col-md-3\">
    Kodi i produktit
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\" type=\"text\" name=\"Pro_Code\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Code", array()), "html", null, true);
        echo "\" >
    </div>        
    
    
    <div class=\"col-md-3\">
    Pershkrimi i produktit
    </div>
    <div class=\"col-md-9\">
    <textarea rows=\"8\" class=\"form-control\" name=\"Pro_Description\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Description", array()), "html", null, true);
        echo "</textarea>
    </div>        
     
     
         <div class=\"col-md-3\">
    Cmimi
    </div>
    <div class=\"col-md-9\">
    <input class=\"form-control\" type=\"text\" name=\"Pro_Price\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produkti"]) ? $context["produkti"] : null), "Pro_Price", array()), "html", null, true);
        echo "\" >
    </div>      
    <input type=\"button\" class=\"btn btn-success\" value=\"Regjistro\">
    </form>
</div>
</div>   
";
    }

    public function getTemplateName()
    {
        return "admin/produkti.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 35,  71 => 27,  60 => 19,  50 => 12,  39 => 3,  36 => 2,  11 => 1,);
    }
}
