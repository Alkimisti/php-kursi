-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2015 at 04:03 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ickshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `Usr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usr_Email` varchar(50) NOT NULL,
  `Usr_Password` varchar(40) NOT NULL,
  `Usr_Name` varchar(20) NOT NULL,
  `Usr_Level` int(11) NOT NULL DEFAULT '0',
  `Usr_Salt` int(11) NOT NULL,
  PRIMARY KEY (`Usr_ID`),
  UNIQUE KEY `Usr_Email` (`Usr_Email`),
  KEY `Usr_Password` (`Usr_Password`),
  KEY `Usr_Level` (`Usr_Level`),
  KEY `Usr_Salt` (`Usr_Salt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Usr_ID`, `Usr_Email`, `Usr_Password`, `Usr_Name`, `Usr_Level`, `Usr_Salt`) VALUES
(1, 'tahir.hoxha@gmail.com', 'cf9be0086bf4502449a6a933fa5a784593cbf5e7', 'Tahir Hoxha', 2, 8601989),
(2, 'korab@korab.com', '98c5a3012cfb0a19ec1da2ba462cbecc6bceb7fd', 'Korab', 1, 7538787),
(3, 'zanfina@hotmail.com', '393c1bab533f7df51344d8600ce81ef3e7716248', 'Zanfina', 0, 7368225);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
