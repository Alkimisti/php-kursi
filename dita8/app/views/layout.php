<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?=isset($title)?$title:"Untitled";?></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="assets/css/stili.css">
</head>

<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>Online Shop</h1>
<nav>
<ul>
<li><a href='/'>Home</a></li>

<li><a href='/login'>Login</a></li>
<li><a href='/produktet'>Produktet</a></li>
<li><a href='/profili'>Profili</a></li>
<li><a href='/kontakti'>Kontakti</a></li>
<li><a href='/logout'>Logout</a></li>
<li><a href='/signup'>Regjistrohu</a></li>
</ul>
</nav>
<?php
$template = "../app/views/".$view.".php";
if (file_exists($template))
	{
	include($template);
	}
else
	{
		echo "<p>Nuk ekziston kjo faqe!</p>";
	}

?>
</div>
</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js">
</body>
</html>