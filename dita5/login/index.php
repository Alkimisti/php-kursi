<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="stili.css">
</head>

<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<h1>Online Shop</h1>
<nav>
<ul>
<li><a href='index.php'>Home</a></li>

<li><a href='index.php?cmd=login'>Login</a></li>
<li><a href='index.php?cmd=profili'>Profili</a></li>
<li><a href='index.php?cmd=kontakti'>Kontakti</a></li>
<li><a href='index.php?cmd=logout'>Logout</a></li>
<li><a href='index.php?cmd=signup'>Regjistrohu</a></li>
</ul>
</nav>
<?php
define("THIRRJA", 1);
session_start();
require_once("db.php");

if (isset($_GET['cmd']))
	{
	$cmd = filter_var($_GET['cmd'], FILTER_SANITIZE_STRING);
	
	switch ($cmd)
		{
			case 'login':
				require 'login.php';
				break;
			case 'check':
				require 'check.php';
				break;				
			case 'kontakti':
				require 'kontakti.php';
				break;
			case 'profili':
				require 'profili.php';
				break;
			case 'logout':
				require 'logout.php';
				break;
			case 'signup':
				require 'regjistrimi.php';
				break;							
			default:
				echo "Nuk ekziston kjo faqe!";
		}
	}
else
	{
	require 'home.php';	
	}



?>
</div>
</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js">
</body>
</html>