<?php

$a = new Test;
echo  "<p>counter=".$a::counter()."</p>";
echo  "<p>number=".$a->number."</p>";

$b = new Test;
echo  "<p>counter=".$b::counter()."</p>";
echo  "<p>number=".$b->number."</p>";

$c = new Test;
echo  "<p>counter=".$c::counter()."</p>";
echo  "<p>number=".$c->number."</p>";


class Test {
	private static $count = 0;
	public $number;
	
	public function __construct() {
		$this->number = 1;	
	}
	public static function counter() {
		self::$count++;
		return self::$count."<br>";
		
	}
	
}