<?php

class Product {
	public $cmimi;
	public $sasia;
	
	function __construct($sasia, $cmimi) {
		$this->cmimi = $cmimi;
		$this->sasia = $sasia;
	}
	
	public function subtotal() {
		return $this->cmimi * $this->sasia;
	}
	
	public static function subtotal2($cmimi, $sasia) {
		return $cmimi * $sasia;
	}
	
	
}