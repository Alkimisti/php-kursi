<?php
namespace Baz;
class Shporta {
	public $prod_obj;
	public $qty = array();
	public $products;
	public $new_qty = 0;
	
	function __construct() {
			 $this->products = array();
	}
	
	// instance injection 
	public function addProduct(\Foo\Bar2\Produkti $prod_obj, $qty) {
		
		$i = $prod_obj->Pro_ID;
		if (isset($this->products[$i])) {
			$this->products[$i]['qty'] += $qty;
		} else {
			$this->products[$i]['qty'] = $qty;	
		}
		$this->products[$i]['name'] = $prod_obj->getName();
		$this->products[$i]['price'] = $prod_obj->getPrice();
		$this->products[$i]['subtotal'] = $prod_obj->getPrice()*$this->products[$i]['qty'];

		return;		
	}
	
	public function showCart() {
		$shuma = 0;
		$tab = "<table border='1'>";
		$tab .= "<tr><td>ID</td>
			<td>Emri</td>
			<td>Sasia</td>
			<td>Cmimi</td>
			<td>Vlera</td></tr>";
		foreach($this->products as $key=>$value) {
			$tab .= "<tr>
			<td>".$key."</td>
			<td>".$value['name']."</td>
			<td>".$value['qty']."</td>
			<td>".$value['price']."</td>
			<td>".$value['subtotal']."</td>
			</tr>";	
			$shuma += $value['subtotal'];
		}	
		$tab .= "<tr><td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>".$shuma."</td></tr>";		
		$tab .= "</table>";
		
		return $tab;
		
	}
	

	

}