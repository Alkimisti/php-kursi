<?php
// namespace, autoload, object
function __autoload($class_name) {
    include $class_name . '.php'; }

use Baz\Shporta as Cart;
use Foo\Bar2\Produkti as Produkti;

$c = new Cart;

$blerje = rand(1, 5);
echo "<p>Blerje: ".$blerje."</p>";
for ($i=1; $i<=$blerje; $i++) {
	$id = rand(1,3);
	echo "<p>".$i.") Produkti: ".$id;
	// $p = new Produkti($id);
	$p = new Produkti($id);
	// $p = new Subf\Produkti($id);
	$sasia = rand(1,10);
	echo " Sasia: ".$sasia."</p>";
	$c->addProduct($p, $sasia);
}

echo $c->showCart();
