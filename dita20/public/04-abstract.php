<?php
abstract class FrutiAbstrakt {

	public function __construct() {
	
	}	
	
	public function kendo() {
		echo "<p>Xhamadani vija vija!</p>";
	}
	
	abstract protected function shkruaj();
}

class Fruti extends FrutiAbstrakt {
	
	
	public function fol() {
		echo "<hr>";
		echo "<p>Une jam Fruti!</p>";	
	}
	
	 protected function shkruaj() {
	 echo "<p>-----</p>";
		echo "<p>Po shkruaj!</p>";	
	}			
}

class Molla extends Fruti {
	
	public function fol() {
		echo "<hr>";
		echo "<p>Une jam Molla, e bija e Frutit!</p>";
		$this->shkruaj();	
	}	
}

class Dardha extends Fruti {
	
	public function fol() {
		echo "<hr>";
		echo "<p>Une jam Dardha, e bija e Frutit!</p>";	
		$this->shkruaj();
	}	
	

	
	public function kendo() {
		echo "<p>Nuk di te kendoj!</p>";
	}	
}


/* 
// Objekti nuk mund te instancohet nga klasa abstrakte
$fruti = new FrutiAbstrakt;
$fruti->fol();
$fruti->kendo();
*/

$fruti1 = new Fruti;
$fruti1->fol();
$fruti1->kendo();
// shkruaj() eshte protected, nuk mund te thirret nga jashte klases dhe femijeve te tij
//$fruti1->shkruaj();

$fruti2 = new Molla;
$fruti2->fol();
$fruti2->kendo();


$fruti3 = new Dardha;
$fruti3->fol();
$fruti3->kendo();


