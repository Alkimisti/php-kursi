<?php
namespace Foo\Bar2;
class Produkti {
	public $Pro_ID;
	private $Pro_Price;
	private $lista;
	
	function __construct($Pro_ID=1) {
		$this->Pro_ID = $Pro_ID;
		
		// Lista e produkteve, simulim i DB
		$this->lista = array(
		1=>array('name'=>"Inspiron 17R", 'price'=>400),
		2=>array('name'=>"Latitude 300", 'price'=>300),
		3=>array('name'=>"Inspiron 15R", 'price'=>300));	
	}
	
	public function subtotal() {
		return $this->price * $this->quantity;
	}
	
	public function getPrice() {
		if (isset($this->lista[$this->Pro_ID]['price'])) {
			
			return $this->lista[$this->Pro_ID]['price'];
		} else {
		return 0;	
		}
	}
	
	public function getName() {	
		return $this->lista[$this->Pro_ID]['name'];
	}
}