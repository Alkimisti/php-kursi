<?php
interface  Driver {
	public function open($name);
	public function read();
	public function write();
	public function close();
}


class Text implements Driver {
	
	public function open($n) {
		return "Open the file ".$n;
	}
	
	public function read() {
		return "Read from file";	
	}
	
	public function write() {
		return "Write into file";	
	}
	
	public function close() {
		return "Close the file";
		
	}
		
}

class Database implements Driver {
	
	public function open($n) {
		return "Open the DB ".$n;
	}
	
	public function read() {
		return "Read from DB";	
	}
	
	public function write() {
		return "Write into DB";	
	}
	
	public function close() {
		return "Close the DB";
		
	}
		
}

$op1 = new Text;
echo "<p>".$op1->open("test.txt");
echo "<p>".$op1->read();
echo "<p>".$op1->write();
echo "<p>".$op1->close();

$op2 = new Database;
echo "<p>".$op2->open("test.db");
echo "<p>".$op2->read();
echo "<p>".$op2->write();
echo "<p>".$op2->close();
