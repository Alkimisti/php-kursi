<?php

$fruti = new Fruti;
$fruti1 = new Molla;
$fruti2 = new Dardha;
$fruti3 = new Portokalli;

class Fruti {

	public function __construct() {
		echo "<hr>";
		echo "<p>Une jam Fruti!</p>";	
	}	
}

class Molla extends Fruti {
	
	public function __construct() {
		echo "<hr>";
		echo "<p>Une jam Molla, e bija e Frutit!</p>";	
	}	
}

class Dardha extends Fruti {
	
	public function __construct() {
		echo "<hr>";
		echo "<p>Une jam Dardha, e bija e Frutit!</p>";	
	}	
}

class Portokalli extends Fruti {
	public function __construct() {
		parent::__construct();
		echo "<p>Une jam Portokalli, i biri i Frutit!</p>";	
	}
}