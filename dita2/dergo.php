<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<?php

var_dump($_POST);

$nx_emri = trim(filter_var($_POST['emri'], FILTER_SANITIZE_STRING));
if (strlen($nx_emri) == 0)
	{
	echo "<p>Nuk e keni shenuar emrin";	
	}

$nx_fjalekalimi = $_POST['fjalekalimi'];
$nx_mosha = intval($_POST['mosha']);
$nx_emaili = $_POST['emaili'];

$anetari_id = $_GET['anetari'];
if (isset($_POST['duhan']) && $_POST['duhan']=="on")
	{
		$duhan = "Pin duhan";
	}
else
	{
		$duhan = "Nuk pin duhan";	
	}
	
$webadresa = $_POST['webadresa'];
if (!filter_var($webadresa, FILTER_VALIDATE_URL))
	{
	echo "<p>Web adrese jovalide";	
	}

$komenti = strip_tags($_POST['komenti'], "<h1><p><i><b>");	
$komenti = substr($komenti, 0, 20);
echo "<p>Emri juaj: ".$nx_emri;
echo "<p>Fjalekalimi: ".$nx_fjalekalimi;
echo "<p>Mosha: ".$nx_mosha;
if ($nx_mosha === 0)
	{
	echo "<p>Nuk keni shenuar vlere numerike te mosha!";	
	}
echo "<p>ID: ".$anetari_id;
echo "<p>Emaili: ".$nx_emaili;

if (!filter_var($nx_emaili, FILTER_VALIDATE_EMAIL))
	{
	echo "<p>Emaili nuk eshte i shkruar mire";	
	}

echo "<p>A pin duhan: ".$duhan;
echo "<div>".nl2br($komenti)."</div>";
?>
</body>
</html>
